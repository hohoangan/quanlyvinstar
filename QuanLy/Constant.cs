﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLy
{
    public static class Constant
    {
        public static double windowHeight = 600;
        public static int Permission = 0;
        public static List<ComboboxString> TinhTrangNhanVien()
        {
            //Đang thử việc | Đã kí HĐLĐ | Đã nghỉ | Thực tập sinh | Cộng tác viên | Khác
            List<ComboboxString> lst = new List<ComboboxString>();
            lst.Add(new ComboboxString { Name = "Đang thử việc" });
            lst.Add(new ComboboxString { Name = "Đã kí HĐLĐ" });
            lst.Add(new ComboboxString { Name = "Đã nghỉ" });
            lst.Add(new ComboboxString { Name = "Thực tập sinh" });
            lst.Add(new ComboboxString { Name = "Cộng tác viên" });
            lst.Add(new ComboboxString { Name = "Gửi thư nhận việc" });
            lst.Add(new ComboboxString { Name = "Không nhận việc" });
            lst.Add(new ComboboxString { Name = "Khác" });
            return lst;
        }
        public static List<ComboboxString> ThoiGianNghiPhep()
        {
            //Morning | Afternoon | AllDay
            List<ComboboxString> lst = new List<ComboboxString>();
            lst.Add(new ComboboxString { Name = "Sáng" });
            lst.Add(new ComboboxString { Name = "Chiều" });
            lst.Add(new ComboboxString { Name = "Cả ngày" });
            return lst;
        }
        public static List<ComboboxString> TinhTrangHoSo()
        {
            //Photo | Chính | Chứng thực
            List<ComboboxString> lst = new List<ComboboxString>();
            lst.Add(new ComboboxString { Name = "Photo"});
            lst.Add(new ComboboxString { Name = "Bản Chính" });
            lst.Add(new ComboboxString { Name = "Chứng thực" });
            return lst;
        }
        public static List<ComboboxString> LoaiHD()
        {
            //Photo | Chính | Chứng thực
            List<ComboboxString> lst = new List<ComboboxString>();
            lst.Add(new ComboboxString { Name = "HĐHV" });
            lst.Add(new ComboboxString { Name = "HĐTV" });
            lst.Add(new ComboboxString { Name = "HĐLĐ" });
            lst.Add(new ComboboxString { Name = "HĐLĐ NoLtd" });
            lst.Add(new ComboboxString { Name = "HĐLĐ Limited"});
            lst.Add(new ComboboxString { Name = "HĐTV TECH" });
            lst.Add(new ComboboxString { Name = "HĐLĐ TECH" });
            lst.Add(new ComboboxString { Name = "HĐ TUVAN" });
            lst.Add(new ComboboxString { Name = "Offer Letter" });
            lst.Add(new ComboboxString { Name = "Thực Tập" });
            lst.Add(new ComboboxString { Name = "Cộng Tác Viên" });
            return lst;
        }
        public static List<ComboboxString> MyDayOfWeek()
        {
            //Photo | Chính | Chứng thực
            List<ComboboxString> lst = new List<ComboboxString>();
            lst.Add(new ComboboxString {Id =DayOfWeek.Monday, Name = "Monday" });
            lst.Add(new ComboboxString {Id =DayOfWeek.Tuesday, Name = "Tuesday" });
            lst.Add(new ComboboxString {Id =DayOfWeek.Wednesday, Name = "Wednesday" });
            lst.Add(new ComboboxString {Id =DayOfWeek.Thursday, Name = "Thursday" });
            lst.Add(new ComboboxString {Id =DayOfWeek.Friday, Name = "Friday" });
            lst.Add(new ComboboxString {Id =DayOfWeek.Saturday, Name = "Saturday" });
            return lst;
        }
        public static List<ComboboxString> HinhThucUngTuyen()
        {
            //Photo | Chính | Chứng thực
            List<ComboboxString> lst = new List<ComboboxString>();
            lst.Add(new ComboboxString { Name = "Tự ứng tuyển" });
            lst.Add(new ComboboxString { Name = "Được mời ứng tuyển" });
            return lst;
        }
        public static List<ComboboxString2> LamThu7()
        {
            //Photo | Chính | Chứng thực
            List<ComboboxString2> lst = new List<ComboboxString2>();
            //0: ko, 1:lam, 2: lam 1/2
            lst.Add(new ComboboxString2 { Name = "Không làm T7", Id = 0 });
            lst.Add(new ComboboxString2 { Name = "Làm cả ngày T7", Id = 1 });
            lst.Add(new ComboboxString2 { Name = "Làm nữa ngày T7", Id = 2 });
            return lst;
        }
    }

    public class ComboboxString{
        public DayOfWeek Id { get; set; }
        public string Name { get; set; }
    }
    public class ComboboxString2
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
