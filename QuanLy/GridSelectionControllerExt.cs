﻿using Syncfusion.Data.Extensions;
using Syncfusion.UI.Xaml.Grid;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace QuanLy
{
    public class GridSelectionControllerExt : GridSelectionController
    {
        public GridSelectionControllerExt(SfDataGrid grid)
            : base(grid)
        {
        }
        protected override void ProcessKeyDown(KeyEventArgs args)
        {
            //Customizes the Delete key operation.
            if (args.Key == Key.Delete)
            {
                //Gets the cell value of current column.
                var record = this.DataGrid.View.Records[this.DataGrid.SelectedIndex].Data;
                int columIndex = this.CurrentCellManager.CurrentCell.ColumnIndex - 1;// vi trừ đi cột STT vị trí bị sai 1 đơn vị
                var cellVal = this.DataGrid.View.GetPropertyAccessProvider().GetValue(record, this.DataGrid.Columns[columIndex].MappingName);
                //Returns the cell value when the current column's cell is not set to null.
                if (cellVal != null)
                {
                    var columnName = this.DataGrid.Columns[columIndex].MappingName; PropertyDescriptorExtensions.SetValue(this.DataGrid.View.GetItemProperties(), record, null, columnName);
                }
            }
            else
                base.ProcessKeyDown(args);
        }
    }
}
