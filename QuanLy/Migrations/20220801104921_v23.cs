﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanLy.Migrations
{
    public partial class v23 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "AllowanceCommunicate",
                table: "StaffContracts",
                type: "REAL",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "AllowanceHouse",
                table: "StaffContracts",
                type: "REAL",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "AllowanceKPI",
                table: "StaffContracts",
                type: "REAL",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "AllowanceOther",
                table: "StaffContracts",
                type: "REAL",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AllowanceCommunicate",
                table: "StaffContracts");

            migrationBuilder.DropColumn(
                name: "AllowanceHouse",
                table: "StaffContracts");

            migrationBuilder.DropColumn(
                name: "AllowanceKPI",
                table: "StaffContracts");

            migrationBuilder.DropColumn(
                name: "AllowanceOther",
                table: "StaffContracts");
        }
    }
}
