﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanLy.Migrations
{
    public partial class v25 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LamThu7",
                table: "Shifts",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LamThu7",
                table: "Shifts");
        }
    }
}
