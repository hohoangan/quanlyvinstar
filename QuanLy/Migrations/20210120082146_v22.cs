﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanLy.Migrations
{
    public partial class v22 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "OffsetDays");

            migrationBuilder.AddColumn<int>(
                name: "StaffId",
                table: "OffsetDays",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StaffId",
                table: "OffsetDays");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "OffsetDays",
                type: "TEXT",
                nullable: true);
        }
    }
}
