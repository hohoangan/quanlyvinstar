﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanLy.Migrations
{
    public partial class v17 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_CandidateQuestions_CandidateId",
                table: "CandidateQuestions");

            migrationBuilder.CreateIndex(
                name: "IX_Candidate_QuestionId",
                table: "CandidateQuestions",
                columns: new[] { "CandidateId", "QuestionId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Candidate_QuestionId",
                table: "CandidateQuestions");

            migrationBuilder.CreateIndex(
                name: "IX_CandidateQuestions_CandidateId",
                table: "CandidateQuestions",
                column: "CandidateId");
        }
    }
}
