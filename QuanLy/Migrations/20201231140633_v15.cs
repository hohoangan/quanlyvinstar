﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanLy.Migrations
{
    public partial class v15 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Candidate_Step_Status",
                table: "CandidateProcesses");

            migrationBuilder.CreateTable(
                name: "Holidays",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Date = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Holidays", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Candidate_Step_Status",
                table: "CandidateProcesses",
                columns: new[] { "CandidateId", "CandidateStepId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Holidays");

            migrationBuilder.DropIndex(
                name: "IX_Candidate_Step_Status",
                table: "CandidateProcesses");

            migrationBuilder.CreateIndex(
                name: "IX_Candidate_Step_Status",
                table: "CandidateProcesses",
                columns: new[] { "CandidateId", "CandidateStatusId", "CandidateStepId" },
                unique: true);
        }
    }
}
