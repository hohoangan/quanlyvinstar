﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using QuanLy;

namespace QuanLy.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20201209110633_v2")]
    partial class v2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "5.0.0");

            modelBuilder.Entity("QuanLy.Models.Company", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("AuthorizationLetter")
                        .HasColumnType("TEXT");

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("FromDate")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("Phone")
                        .HasColumnType("TEXT");

                    b.Property<string>("Position")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("Representative")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("TaxNo")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<DateTime?>("ToDate")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Companies");
                });

            modelBuilder.Entity("QuanLy.Models.Department", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Departments");
                });

            modelBuilder.Entity("QuanLy.Models.DownTime", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Des")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("FromDate")
                        .HasColumnType("TEXT");

                    b.Property<string>("Location")
                        .HasColumnType("TEXT");

                    b.Property<bool>("MissPunch")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Reason")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int>("StaffId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Time")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<DateTime?>("ToDate")
                        .HasColumnType("TEXT");

                    b.Property<double>("TotalDay")
                        .HasColumnType("REAL");

                    b.HasKey("Id");

                    b.HasIndex("StaffId");

                    b.ToTable("DownTimes");
                });

            modelBuilder.Entity("QuanLy.Models.Manager", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("DepartmentId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("StaffId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("DepartmentId");

                    b.HasIndex("StaffId");

                    b.ToTable("Manager");
                });

            modelBuilder.Entity("QuanLy.Models.Position", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("DepartmentId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("DepartmentId");

                    b.ToTable("Positions");
                });

            modelBuilder.Entity("QuanLy.Models.Shift", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<TimeSpan>("End")
                        .HasColumnType("TEXT");

                    b.Property<TimeSpan>("Start")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Shifts");
                });

            modelBuilder.Entity("QuanLy.Models.Staff", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Address")
                        .HasColumnType("TEXT");

                    b.Property<string>("AddressNow")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("BHXH")
                        .HasColumnType("TEXT");

                    b.Property<string>("BankName")
                        .HasColumnType("TEXT");

                    b.Property<string>("BankNo")
                        .HasColumnType("TEXT");

                    b.Property<string>("BikeType")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("BirthDay")
                        .HasColumnType("TEXT");

                    b.Property<string>("CMND")
                        .HasColumnType("TEXT");

                    b.Property<string>("CMNDAdress")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("CMNDCreatedDate")
                        .HasColumnType("TEXT");

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("Country")
                        .HasColumnType("TEXT");

                    b.Property<string>("Des")
                        .HasColumnType("TEXT");

                    b.Property<string>("Email")
                        .HasColumnType("TEXT");

                    b.Property<string>("FullName")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<DateTime?>("ParkingFromDate")
                        .HasColumnType("TEXT");

                    b.Property<string>("ParkingPlace")
                        .HasColumnType("TEXT");

                    b.Property<DateTime?>("ParkingToDate")
                        .HasColumnType("TEXT");

                    b.Property<string>("Phone")
                        .HasColumnType("TEXT");

                    b.Property<string>("PlateNo")
                        .HasColumnType("TEXT");

                    b.Property<int>("ShiftId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Status")
                        .HasColumnType("TEXT");

                    b.Property<string>("TaxNo")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("ShiftId");

                    b.ToTable("Staffs");
                });

            modelBuilder.Entity("QuanLy.Models.StaffContract", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<double>("AllowanceGas")
                        .HasColumnType("REAL");

                    b.Property<double>("AllowanceLunch")
                        .HasColumnType("REAL");

                    b.Property<double>("AllowancePhone")
                        .HasColumnType("REAL");

                    b.Property<double>("BasicSalary")
                        .HasColumnType("REAL");

                    b.Property<double>("Bonus")
                        .HasColumnType("REAL");

                    b.Property<int>("CompanyId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("ContractNo")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("ContractType")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("FromDate")
                        .HasColumnType("TEXT");

                    b.Property<int>("PositionId")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("SignatureDate")
                        .HasColumnType("TEXT");

                    b.Property<int>("StaffId")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime?>("ToDate")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("PositionId");

                    b.HasIndex("StaffId");

                    b.ToTable("StaffContracts");
                });

            modelBuilder.Entity("QuanLy.Models.StaffFile", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("BangCap")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("CMND")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<bool>("Done")
                        .HasColumnType("INTEGER");

                    b.Property<string>("GiayKhamSucKhoe")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("SoHK")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("SoYeuLyLich")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int>("StaffId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("StaffId");

                    b.ToTable("StaffFiles");
                });

            modelBuilder.Entity("QuanLy.Models.DownTime", b =>
                {
                    b.HasOne("QuanLy.Models.Staff", "Staff")
                        .WithMany()
                        .HasForeignKey("StaffId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Staff");
                });

            modelBuilder.Entity("QuanLy.Models.Manager", b =>
                {
                    b.HasOne("QuanLy.Models.Department", "Department")
                        .WithMany("Managers")
                        .HasForeignKey("DepartmentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("QuanLy.Models.Staff", "Staff")
                        .WithMany("Managers")
                        .HasForeignKey("StaffId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Department");

                    b.Navigation("Staff");
                });

            modelBuilder.Entity("QuanLy.Models.Position", b =>
                {
                    b.HasOne("QuanLy.Models.Department", "Department")
                        .WithMany("Positions")
                        .HasForeignKey("DepartmentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Department");
                });

            modelBuilder.Entity("QuanLy.Models.Staff", b =>
                {
                    b.HasOne("QuanLy.Models.Shift", "Shift")
                        .WithMany("Staffs")
                        .HasForeignKey("ShiftId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Shift");
                });

            modelBuilder.Entity("QuanLy.Models.StaffContract", b =>
                {
                    b.HasOne("QuanLy.Models.Company", "Company")
                        .WithMany("StaffContracts")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("QuanLy.Models.Position", "Position")
                        .WithMany("StaffContracts")
                        .HasForeignKey("PositionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("QuanLy.Models.Staff", "Staff")
                        .WithMany("StaffContracts")
                        .HasForeignKey("StaffId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Company");

                    b.Navigation("Position");

                    b.Navigation("Staff");
                });

            modelBuilder.Entity("QuanLy.Models.StaffFile", b =>
                {
                    b.HasOne("QuanLy.Models.Staff", "Staff")
                        .WithMany()
                        .HasForeignKey("StaffId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Staff");
                });

            modelBuilder.Entity("QuanLy.Models.Company", b =>
                {
                    b.Navigation("StaffContracts");
                });

            modelBuilder.Entity("QuanLy.Models.Department", b =>
                {
                    b.Navigation("Managers");

                    b.Navigation("Positions");
                });

            modelBuilder.Entity("QuanLy.Models.Position", b =>
                {
                    b.Navigation("StaffContracts");
                });

            modelBuilder.Entity("QuanLy.Models.Shift", b =>
                {
                    b.Navigation("Staffs");
                });

            modelBuilder.Entity("QuanLy.Models.Staff", b =>
                {
                    b.Navigation("Managers");

                    b.Navigation("StaffContracts");
                });
#pragma warning restore 612, 618
        }
    }
}
