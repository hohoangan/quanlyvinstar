﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLy
{
    /// <summary>
    /// Interaction logic for PopUp.xaml
    /// </summary>
    public partial class PopUp : Window
    {
        public PopUp()
        {
            InitializeComponent();
        }
        public PopUp(string mess, string min)
        {
            InitializeComponent();
            lbMess.Text = mess;
            lbMin.Content = string.Format("Còn {0} phút", min);
            Load();
        }

        void Load()
        {
            try
            {
                double W = System.Windows.SystemParameters.PrimaryScreenWidth;
                double H = System.Windows.SystemParameters.PrimaryScreenHeight;
                this.Top = H - this.Height - 100;
                this.Left = W - this.Width - 100;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
