﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.ComponentModel.DataAnnotations;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Security.Cryptography;

namespace QuanLy
{
    public static class Helps
    {
        public static FrameworkElement GetWindowParent(UserControl p)
        {
            FrameworkElement parent = p;
            while (parent.Parent != null)
            {
                parent = parent.Parent as FrameworkElement;
            }
            return parent;
        }

        public static string GetBasePath(string fileName = "")
        {
            //string dir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            string dir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string folder = System.IO.Path.Combine(dir, "Template");
            if (!string.Empty.Equals(fileName))
                folder = Path.Combine(folder, fileName);
            return folder;
        }

        public static BitmapFrame GetImage(string fileName = "")
        {
            string path = GetBasePath(fileName);
            Uri iconUri = new Uri(path, UriKind.RelativeOrAbsolute);
            return BitmapFrame.Create(iconUri);
        }

        public static void FindAndReplace(Microsoft.Office.Interop.Word.Application wordApp, object toFindText, object replaceWithText)
        {
            try
            {
                object matchCase = true;
                object matchWholeWord = true;
                object matchWildCards = false;
                object matchSoundLike = false;
                object nmatchAllforms = false;
                object forward = true;
                object format = false;
                object matchKashida = false;
                object matchDiactitics = false;
                object matchAlefHamza = false;
                object matchControl = false;
                object read_only = false;
                object visible = true;
                object replace = 2;
                object wrap = 1;

                wordApp.Selection.Find.Execute(ref toFindText,
                    ref matchCase, ref matchWholeWord,
                    ref matchWildCards, ref matchSoundLike,
                    ref nmatchAllforms, ref forward,
                    ref wrap, ref format, ref replaceWithText,
                    ref replace, ref matchKashida,
                    ref matchDiactitics, ref matchAlefHamza,
                    ref matchControl);
            }
            catch (Exception)
            {

            }
        }
        
        public static string GetMd5Hash(string input)
        {
            using MD5 md5Hash = MD5.Create();
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
    }

    public class Id : ValidationAttribute
    {
        protected override System.ComponentModel.DataAnnotations.ValidationResult IsValid(
            object value,
            ValidationContext validationContext)
        {
            return Convert.ToInt32(value) > 0 ?
                System.ComponentModel.DataAnnotations.ValidationResult.Success :
                new System.ComponentModel.DataAnnotations.ValidationResult($"The {validationContext.DisplayName} field is required.");
        }
    }

    public class price : ValidationAttribute
    {
        protected override System.ComponentModel.DataAnnotations.ValidationResult IsValid(
            object value,
            ValidationContext validationContext)
        {
            return Convert.ToDouble(value) > 0 ?
                System.ComponentModel.DataAnnotations.ValidationResult.Success :
                new System.ComponentModel.DataAnnotations.ValidationResult($"The {validationContext.DisplayName} field is required.");
        }
    }
    
}
