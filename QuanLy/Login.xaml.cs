﻿using Syncfusion.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLy
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        string pwd = "";
        public Login()
        {
            InitializeComponent();
            try
            {
                string content = System.IO.File.ReadAllText(@"permission.txt");
                var arr = content.Split("\r\n");
                Dictionary<string, string> dir = new Dictionary<string, string>();
                arr?.ForEach(item =>
                {
                    var v = item.Split(":");
                    dir.Add(v[0], v[1]);
                });
                pwd = dir.GetValueOrDefault("PWD");
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnGuest_Click(object sender, RoutedEventArgs e)
        {
            Constant.Permission = 0;
            var mainWindow = new MainWindow();
            mainWindow.Show();
            mainWindow.Closed += delegate
            {
                this.Close();
            };
            this.Hide();
        }

        private void btnAdmin_Click(object sender, RoutedEventArgs e)
        {
            string passwords = Helps.GetMd5Hash(txtPasswords.Password);
            if (passwords.Equals(pwd))
            {
                Constant.Permission = 1;
                var mainWindow = new MainWindow();
                mainWindow.Closed += delegate
                {
                    this.Close();
                };
                mainWindow.Show();
                txtPasswords.Password = "";
                this.Hide();
            }
            else
            {
                MessageBox.Show("Sai mật mã");
            }
        }
    }
}
