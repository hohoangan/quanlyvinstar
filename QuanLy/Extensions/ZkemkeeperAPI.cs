﻿using QuanLy.Extensions;
using QuanLy.Models;
using QuanLy.ModelViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLy.Extensions
{
    public class ZkemkeeperAPI
    {
        ViewModel viewModel;
        #region Connect may cham cong zkemkeeper
        //https://stackoverflow.com/questions/35666583/how-to-connect-attendance-punching-machine-using-zkemkeeper-in-c
        //https://stackoverflow.com/questions/31377437/zkemkeeper-dll-support-which-biometric-devices/31793398#31793398
        /**
         * 1. cai dat dll
         * xem link https://www.youtube.com/watch?v=f_CHbUWu69U&ab_channel=GabrielMolina
         * tải file về bỏ vào 2 folder System32 (64 bits thì bỏ vào SysWOW64).
         * run cmd khai bao với quyền admin mới đc: regsvr32 ~\system32\zkemkeeper.dll 
         * tải zkemkeeper dll từ nuget
         * NOTE: chinh buil về Debug x86 (trong BUILD -> Configmanagement)
        */
        #endregion
        //public zkemkeeper.CZKEM axCZKEM1;
        public List<MCC> listMCC = new List<MCC>();
        public List<FileChamCong> lstFileChamCong = new List<FileChamCong>();
        public ZkemkeeperAPI()
        {
            try
            {
                viewModel = new ViewModel();
                lstFileChamCong = Load();
            }
            catch (Exception)
            { 
            }
        }
        public List<MCC> GetData()
        {
            try
            {
                string dwEnrollNumber = "";
                int dwMachineNumber = 0;
                string Password = "";
                string Name = "";
                bool dwEnable = false;
                int dwVerifyMode = 0;
                int dwInOutMode = 0;
                int dwWorkcode = 0;

                int dwYear = 0;
                int dwMonth = 0;
                int dwDay = 0;
                int dwHour = 0;
                int dwMinute = 0;
                int dwSecond = 0;

                //bool dwEnable = true;
                int Privilege = 1;
                if (ChamCongConstant.axCZKEM1.ReadAllUserID(dwMachineNumber))//read all the attendance records to the memory
                {
                    //xem thong tin của tất cả nhân viên
                    //while (axCZKEM1.SSR_GetAllUserInfo(dwMachineNumber, out dwEnrollNumber, out Name, out Password, out Privilege, out dwEnable))
                    //{
                    //    UserInfoList.Add(new ClsMachineML
                    //    { dwEnrollNumber = dwEnrollNumber, Name = Name, Passwords = Password, Privilege = Privilege, Enabled = dwEnable });
                    //}
                    //lấy thông tin chấm công
                    while (ChamCongConstant.axCZKEM1.SSR_GetGeneralLogData(dwMachineNumber, out dwEnrollNumber, out dwVerifyMode, out dwInOutMode, out dwYear, out dwMonth, out dwDay,
                                        out dwHour, out dwMinute, out dwSecond, ref dwWorkcode))
                    {
                        listMCC.Add(new MCC
                        {
                            Date = new DateTime(dwYear, dwMonth, dwDay),
                            Time = new TimeSpan(dwHour, dwMinute, dwSecond),
                            Code = dwEnrollNumber
                            //dwDay = dwDay,
                            //dwEnrollNumber = dwEnrollNumber,
                            //dwHour = dwHour,
                            //dwInOutMode = dwInOutMode,
                            //dwMachineNumber = dwMachineNumber,
                            //dwMinute = dwMinute,
                            //dwMonth = dwMonth,
                            //dwSecond = dwSecond,
                            //dwVerifyMode = dwVerifyMode,
                            //dwWorkcode = dwWorkcode,
                            //dwYear = dwYear
                        });
                    }
                }
                var model = listMCC.Where(x => x.Date.Year == ChamCongConstant.year && x.Date.Month == ChamCongConstant.month).ToList();
                return model;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        List<FileChamCong> Load()
        {
            try
            {
                //if(ChamCongConstant.bIsConnected == false)
                //    ChamCongConstant.bIsConnected = ChamCongConstant.axCZKEM1.Connect_Net(IPmcc, Port);
                if (!ChamCongConstant.bIsConnected)
                {
                    //MessageBox.Show("Không thể kết nối MCC, kiểm tra lại mạng internet");
                    return null;
                }
                else
                {
                    //lay du lieu
                    listMCC = GetData();
                    //chuan hoa du lieu
                    var chamcong = (from mcc in listMCC.Select(x => { x.Code = x.Code.PadLeft(5, '0'); return x; })
                                    join nv in viewModel.Staffs on mcc.Code equals nv.Code
                                    group new { mcc, nv } by new { mcc.Code, mcc.Date, nv.FullName } into mccG
                                    //let mcc = mccG.FirstOrDefault()
                                    //where mcc != null
                                    select new FileChamCong
                                    {
                                        Code = mccG.Key.Code,
                                        Date = mccG.Key.Date,
                                        DayOfWeek = mccG.Key.Date.DayOfWeek,
                                        FullName = mccG.Key.FullName,
                                        Start = mccG.Min(c => c.mcc.Time),
                                        End = mccG.Max(c => c.mcc.Time),
                                    }).OrderBy(x=>x.Code).ToList();
                    return chamcong;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            finally
            {
                //if (bIsConnected)
                //    axCZKEM1.Disconnect();
            }
        }
    }
}
