﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLy.Extensions
{
    public class ClsMachineML
    {
        public string Id { get; set; }
        public string MachineNo { get; set; }
        public string MachineIP { get; set; }
        public string PortNo { get; set; }
        public string Remark { get; set; }
        public string Tuser { get; set; }
        public string Tdate { get; set; }
        public string Status { get; set; }
        public int dwMachineNumber { get; set; }
        public string dwEnrollNumber { get; set; }
        public int dwVerifyMode { get; set; }
        public int dwInOutMode { get; set; }
        public int dwYear { get; set; }
        public int dwMonth { get; set; }
        public int dwDay { get; set; }
        public int dwHour { get; set; }
        public int dwMinute { get; set; }
        public int dwSecond { get; set; }
        public int dwWorkcode { get; set; }
        public string User_Id { get; set; }
        public string Name { get; set; }
        public int Finger_Index { get; set; }
        public string Finger_Image { get; set; }
        public int Privilege { get; set; }
        public string Passwords { get; set; }
        public bool Enabled { get; set; }
        public int Flag { get; set; }
        public string Fromdate { get; set; }
        public string Todate { get; set; }
    }
}
