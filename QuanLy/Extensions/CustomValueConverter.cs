﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace QuanLy.Extensions
{
    public class CustomValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //while change background color based on condition
            if (value == null || ((value != null) && Excel.ConvertTimeSpanFromString(value.ToString(), "HH:mm:ss") == new TimeSpan(4,0,0)))
                return new SolidColorBrush(Colors.Crimson);
            return new SolidColorBrush(Colors.Transparent);

        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            throw new NotImplementedException();
        }
    }

    public class CustomValueConverterForeground : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //while change background color based on condition
            if (value == null || ((value != null) && Excel.ConvertTimeSpanFromString(value.ToString(), "HH:mm:ss") == new TimeSpan(4, 0, 0)))
                return new SolidColorBrush(Colors.White);
            else if (value == null || ((value != null) && Excel.ConvertTimeSpanFromString(value.ToString(), "HH:mm:ss") >= new TimeSpan(0, 30, 0)))
                return new SolidColorBrush(Colors.Orange);
            return new SolidColorBrush(Colors.White);

        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            throw new NotImplementedException();
        }
    }
}
