﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLy.Extensions
{
    public class MCC
    {
        public string Code { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Time { get; set; }
    }
}
