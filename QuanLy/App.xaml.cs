﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using QuanLy.UserControlUC;
using Syncfusion.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;

namespace QuanLy
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private ServiceProvider serviceProvider;
        public App()
        {
            //ServiceCollection services = new ServiceCollection();
            //services.AddDbContext<AppDbContext>(opt =>
            //{
            //    opt.UseSqlite("Data Source = vinstar.db");
            //});

            //services.AddSingleton<MainWindow>();
            //serviceProvider = services.BuildServiceProvider();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                #region set định dạng thời gian theo local
                var vCulture = new CultureInfo("vi-VN");

                Thread.CurrentThread.CurrentCulture = vCulture;
                Thread.CurrentThread.CurrentUICulture = vCulture;
                CultureInfo.DefaultThreadCurrentCulture = vCulture;
                CultureInfo.DefaultThreadCurrentUICulture = vCulture;

                FrameworkElement.LanguageProperty.OverrideMetadata(
                typeof(FrameworkElement),
                new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));
                #endregion

                #region doc file
                string content = System.IO.File.ReadAllText(@"permission.txt");
                var arr = content.Split("\r\n");
                Dictionary<string, string> dir = new Dictionary<string, string>();
                arr?.ForEach(item =>
                {
                    var v = item.Split(":");
                    dir.Add(v[0], v[1]);
                });

                #endregion
                using (AppDbContext dbContext = new AppDbContext())
                {
                    dbContext.Database.EnsureCreated();
                    string per = dir.GetValueOrDefault("PERMISSION");
                    Constant.Permission = int.Parse(per);
                    //var mainWindow = new MainWindow();
                    //mainWindow.Show();
                    var form = new Login();
                    form.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
