﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using QuanLy.Models;
using System.Linq;
using Syncfusion.UI.Xaml.Grid;
using System.Collections.ObjectModel;
using System.Data;
using QuanLy.ModelViews;
using Microsoft.Extensions.DependencyInjection;
using Syncfusion.Data.Extensions;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for StaffContractUC.xaml
    /// </summary>
    public partial class StaffContractUC : UserControl
    {
        ViewModel viewModel;

        public StaffContractUC()
        {
            InitializeComponent();
            this.gridView.SelectionController = new GridSelectionControllerExt(gridView);
            gridView.Height = Constant.windowHeight;
            Load();
        }

        #region GRID
        void ChangeActionOfDatagrid(bool allow = true)
        {
            gridView.AllowFiltering = allow;
            gridView.AllowSorting = allow;
        }
        #endregion

        #region EVENT
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Load();
        }

        private void Save_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Save();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChangeActionOfDatagrid(false);
                this.gridView.View?.AddNew();
                this.gridView.View?.CommitNew();
            }
            catch { }
        }
        private void gridView_CurrentCellEndEdit(object sender, CurrentCellEndEditEventArgs e)
        {
            var row = gridView.SelectedItem as StaffContract;
            DateTime dtNull = new DateTime(1, 1, 1);
            if("HĐLĐ".Equals(row.ContractType) && dtNull.Equals(row.ToDate))
            {
                row.ToDate = null;
            }
            else if ("HĐTV".Equals(row.ContractType))
            {
                row.ToDate = row.FromDate.AddMonths(2).AddDays(-1);
            }
        }
        #endregion

        #region FUNC
        void Save()
        {
            try
            {
                var staff = gridView.SelectedItem as StaffContract;
                int Id = staff?.Id ?? 0;
                if (Id == 0 && this.gridView.IsAddNewIndex(1))
                {
                    staff = this.gridView.RowGenerator.Items.Find(x => x.Index == 1).RowData as StaffContract;
                }

                if (staff == null || !CheckData(staff))
                    return;

                using (AppDbContext _dbContext = new AppDbContext())
                {
                    if (Id == 0)
                    {
                        _dbContext.StaffContracts.Add(staff);
                        _dbContext.SaveChanges();
                    }
                    else
                    {
                        //update
                        _dbContext.StaffContracts.Update(staff);
                        _dbContext.SaveChanges();
                    }
                }

                MessageBox.Show("Lưu thành công");
                ChangeActionOfDatagrid(true);
                Load();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        bool CheckData(StaffContract data)
        {
            return true;
        }

        void Load()
        {
            ChangeActionOfDatagrid(true);
            try
            {
                //gridView.ClearFilters();
                viewModel = new ViewModel();
                DataContext = viewModel;
                gridView.ItemsSource = viewModel.StaffContracts;
                //cboPosition.ItemsSource = viewModel.Positions.Select(x => new { x.Id, Name = x.Department.Name + " | " + x.Name });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void Copy_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var record = this.gridView.View.Records[this.gridView.SelectedIndex].Data as StaffContract;
                var contract = new StaffContract
                {
                    Id = 0,
                    ContractNo = record.ContractNo + " Copy",
                    BasicSalary = record.BasicSalary,
                    CompanyId = record.CompanyId,
                    ContractType = record.ContractType,
                    DateOfJoinBHXH = record.DateOfJoinBHXH,
                    FromDate = record.FromDate,
                    MonthContract = record.MonthContract,
                    Position = record.Position,
                    Company = record.Company,
                    PositionId = record.PositionId,
                    StaffId = record.StaffId,
                    Staff = record.Staff,
                    ToDate = record.ToDate,
                    AllowanceLunch = record.AllowanceLunch,
                    AllowancePhone = record.AllowancePhone,
                    AllowanceHouse = record.AllowanceHouse,
                    AllowanceGas = record.AllowanceGas,
                    AllowanceKPI = record.AllowanceKPI,
                    AllowanceCommunicate = record.AllowanceCommunicate,
                    AllowanceOther = record.AllowanceOther,
                    Bonus = record.Bonus,
                };
                ChangeActionOfDatagrid(false);
                //this.gridView.View?.AddNew();
                //this.gridView.View?.CommitNew();

                //this.gridView.View.Records[0].Data = contract;

                using (AppDbContext _dbContext = new AppDbContext())
                {
                    _dbContext.StaffContracts.Add(contract);
                    _dbContext.SaveChanges();
                }
                var rowIndex = this.gridView.SelectedIndex;
                Load();
                this.gridView.SelectedItem = this.gridView.View.Records[rowIndex].Data;
                MessageBox.Show("Lưu thành công");
            }
            catch(Exception ex) 
            { 
            }
        }
        #endregion

        private void Del_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var del = gridView.SelectedItem as StaffContract;
                MessageBoxResult rs = MessageBox.Show("Muốn xóa dòng này", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }
                using (AppDbContext context = new AppDbContext())
                {
                    context.StaffContracts.Remove(del);
                    context.SaveChanges();
                }
                MessageBox.Show("Xoá thành công");
                Load();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void btnSaveAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult rs = MessageBox.Show("Muốn lưu tất cả thay đổi", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }
                using (AppDbContext _dbContext = new AppDbContext())
                {
                    var upadte = viewModel.StaffContracts.Where(x => x.Id != 0);
                    var insert = viewModel.StaffContracts.Where(x => x.Id == 0);
                    if (upadte != null)
                    {
                        _dbContext.StaffContracts.UpdateRange(upadte);
                        _dbContext.StaffContracts.UpdateRange(upadte);
                        _dbContext.SaveChanges();
                    }
                    if (insert != null)
                    {
                        //update
                        _dbContext.StaffContracts.AddRange(insert);
                        _dbContext.SaveChanges();
                    }
                }
                MessageBox.Show("Lưu thành công");
                Load();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
    }
}
