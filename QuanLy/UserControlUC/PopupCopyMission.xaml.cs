﻿using QuanLy.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for PopupCopyMission.xaml
    /// </summary>
    public partial class PopupCopyMission : Window
    {
        Mission mission;
        // Define delegate
        public delegate void PassControl(object sender);
        // Create instance (null)
        public PassControl passControl;
        public PopupCopyMission()
        {
            InitializeComponent();
        }
        public PopupCopyMission(Mission mission)
        {
            InitializeComponent();
            this.mission = mission;
            txtApprovalBy.Text = mission.ApprovedBy;
            if (mission.Time.Contains("Cả ngày") == true)//cả ngày
            {
                timepicker.SelectedTime = (new DateTime()).AddTicks(new TimeSpan(8, 0, 0).Ticks);
            }
            else
            {
                timepicker.SelectedTime = (new DateTime()).AddTicks(mission.TotalHours.Ticks);
            }
            fromdate.Text = todate.Text= mission.FromDate.ToString();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (timepicker.SelectedTime == null)
                {
                    MessageBox.Show("Nhập thời gian");
                    timepicker.Focus();
                    return;
                }
                else if (fromdate.SelectedDate == null || todate.SelectedDate == null)
                {
                    MessageBox.Show("Nhập ngày");
                    fromdate.Focus();
                    return;
                }
                using (AppDbContext _dbContext = new AppDbContext())
                {
                    mission.ApprovedBy = txtApprovalBy.Text.Trim();
                    mission.TotalHours = timepicker.SelectedTime.Value.TimeOfDay;
                    DateTime dt = fromdate.SelectedDate.GetValueOrDefault();
                    while(dt <= todate.SelectedDate)
                    {
                        mission.Id = 0;
                        mission.FromDate = dt;
                        _dbContext.Missions.Add(mission);
                        _dbContext.SaveChanges();
                        dt = dt.AddDays(1);
                    }

                    MessageBox.Show("Thêm thành công");
                    if (passControl != null)
                    {
                        passControl(mission);
                    };
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void PresetTimePicker_SelectedTimeChanged(object sender, RoutedPropertyChangedEventArgs<DateTime?> e)
        {

        }
    }
}
