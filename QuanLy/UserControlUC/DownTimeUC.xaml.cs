﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using QuanLy.Models;
using System.Linq;
using Syncfusion.UI.Xaml.Grid;
using System.Collections.ObjectModel;
using System.Data;
using QuanLy.ModelViews;
using Microsoft.Extensions.DependencyInjection;
using Syncfusion.Data.Extensions;
using Microsoft.Win32;
using Syncfusion.UI.Xaml.Grid.Converter;
using System.Diagnostics;
using Syncfusion.XlsIO;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for DownTimeUC.xaml
    /// </summary>
    public partial class DownTimeUC : UserControl
    {
        ViewModel viewModel;
        public DownTimeUC()
        {
            InitializeComponent();
            this.gridView.SelectionController = new GridSelectionControllerExt(gridView);
            gridView.Height = Constant.windowHeight;
            Load();
        }

        #region GRID
        void ChangeActionOfDatagrid(bool allow = true)
        {
            gridView.AllowFiltering = allow;
            gridView.AllowSorting = allow;
        }
        #endregion

        #region EVENT
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Load();
        }

        private void Save_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Save();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChangeActionOfDatagrid(false);
                this.gridView.View?.AddNew();
                this.gridView.View?.CommitNew();
            }
            catch { }
        }
        #endregion

        #region FUNC
        void Save()
        {
            try
            {
                var row = gridView.SelectedItem as DownTime;
                int Id = row?.Id ?? 0;
                if (Id == 0 && this.gridView.IsAddNewIndex(1))
                {
                    row = this.gridView.RowGenerator.Items.Find(x => x.Index == 1).RowData as DownTime;
                }

                if (row == null || !CheckData(row))
                    return;

                using (AppDbContext _dbContext = new AppDbContext())
                {
                    if (Id == 0)
                    {
                        _dbContext.DownTimes.Add(row);
                        _dbContext.SaveChanges();
                    }
                    else
                    {
                        //update
                        _dbContext.DownTimes.Update(row);
                        _dbContext.SaveChanges();
                    }
                }

                MessageBox.Show("Lưu thành công");
                ChangeActionOfDatagrid(true);
                Load();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        bool CheckData(DownTime data)
        {
            if (data.ToDate?.Month - data.FromDate.Month > 0)
                throw new NotImplementedException("Ngày nghỉ không được nằm trong 2 tháng của năm");
            return true;
        }

        void Load()
        {
            ChangeActionOfDatagrid(true);
            try
            {
                ////gridView.ClearFilters();
                viewModel = new ViewModel();
                DataContext = viewModel;
                cboThoiGianNghiPhep.ItemsSource = Constant.ThoiGianNghiPhep();
                cboStaff.ItemsSource = viewModel.Staffs;
                gridView.ItemsSource = viewModel.DownTimes;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        private void btnExcel_Click(object sender, RoutedEventArgs e)
        {
            XuatExcel();
        }
        void XuatExcel()
        {
            try
            {
                if (viewModel?.DownTimes.Count == 0 || gridView.ItemsSource == null)
                    return;
                MessageBoxResult rs = MessageBox.Show("Chọn nơi lưu file", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }

                string filename = string.Empty;
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Excel 97-2003 Workbook|*.xls|Excel Workbook |*.xlsx";
                if (saveFileDialog.ShowDialog() == true)
                {
                    filename = saveFileDialog.FileName;
                    var options = new ExcelExportingOptions();
                    //chọn bỏ cột khi xuất
                    options.ExcludeColumns.Add("Save");
                    options.ExcludeColumns.Add("Del");
                    options.ExcelVersion = ExcelVersion.Excel97to2003;
                    var excelEngine = gridView.ExportToExcel(gridView.View, options);
                    var workBook = excelEngine.Excel.Workbooks[0];
                    workBook.SaveAs(filename);
                    MessageBoxResult rsOK = MessageBox.Show("Đã tạo file thành công!\nCó muốn mở file vừa xuất?", "", MessageBoxButton.OKCancel);
                    if (rsOK.Equals(MessageBoxResult.OK))
                    {
                        Process xlsProcess = new Process();
                        xlsProcess.StartInfo.FileName = filename;
                        xlsProcess.StartInfo.UseShellExecute = true;
                        xlsProcess.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void Del_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var del = gridView.SelectedItem as DownTime;
                MessageBoxResult rs = MessageBox.Show("Muốn xóa dòng này", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }
                using (AppDbContext context = new AppDbContext())
                {
                    context.DownTimes.Remove(del);
                    context.SaveChanges();
                }
                MessageBox.Show("Xoá thành công");
                Load();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void btnSaveAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult rs = MessageBox.Show("Muốn lưu tất cả thay đổi", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }
                using (AppDbContext _dbContext = new AppDbContext())
                {
                    var upadte = viewModel.DownTimes.Where(x => x.Id != 0);
                    var insert = viewModel.DownTimes.Where(x => x.Id == 0);
                    if (upadte != null)
                    {
                        _dbContext.DownTimes.UpdateRange(upadte);
                        _dbContext.SaveChanges();
                    }
                    if (insert != null)
                    {
                        //update
                        _dbContext.DownTimes.AddRange(insert);
                        _dbContext.SaveChanges();
                    }
                }
                MessageBox.Show("Lưu thành công");
                Load();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void Copy_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var record = this.gridView.View.Records[this.gridView.SelectedIndex].Data as DownTime;
                var downtime = new DownTime
                {
                    Id = 0,
                    StaffId = record.StaffId,
                    Des = record.Des,
                    FromDate = record.FromDate,
                    Location = record.Location,
                    MissPunch = record.MissPunch,
                    Reason = record.Reason,
                    Time = record.Time,
                    ToDate = record.ToDate,
                    TotalDay = record.TotalDay
                };
                ChangeActionOfDatagrid(false);
                
                using (AppDbContext _dbContext = new AppDbContext())
                {
                    _dbContext.DownTimes.AddRange(downtime);
                    _dbContext.SaveChanges();
                }
                var rowIndex = this.gridView.SelectedIndex;
                Load();
                //var rowIndex = gridView.ResolveToRowIndex(downtime);
                this.gridView.SelectedItem = this.gridView.View.Records[rowIndex].Data;
                MessageBox.Show("Lưu thành công");
            }
            catch{
            }
        }
    }
}
