﻿using QuanLy.ModelViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for NhanVienUC.xaml
    /// </summary>
    public partial class NhanVienUC : UserControl
    {
        ViewModel viewModel;
        public NhanVienUC()
        {
            InitializeComponent();
            this.gridView.SelectionController = new GridSelectionControllerExt(gridView);
            gridView.Height = Constant.windowHeight;
            Load();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Load();
        }
        void Load()
        {
            ChangeActionOfDatagrid(true);
            try
            {
                //gridView.ClearFilters();
                viewModel = new ViewModel();
                DataContext = viewModel;

                var nhanvien = viewModel.StaffContracts.OrderByDescending(x => x.FromDate).GroupBy(x => x.StaffId).Select(x => x.First());
                var thongtinnhanvien = (from nv in nhanvien
                            join staff in viewModel.Staffs.Where(x => x.Status != "Đã nghỉ") on nv.StaffId equals staff.Id
                            join pos in viewModel.Positions on nv.PositionId equals pos.Id
                            select new
                            {
                                Id = staff.Id,
                                CompanyId = nv.CompanyId,
                                PositionId = pos.Id,
                                StaffId = staff.Id,
                                FullName = staff.FullName,
                                staff.BirthDay,
                                staff.Sex,
                                staff.Phone,
                                staff.Contact1,
                                staff.Contact2,
                            }).OrderBy(x=> new { x.CompanyId, x.PositionId, x.FullName}).ToList();

                gridView.ItemsSource = thongtinnhanvien;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        void ChangeActionOfDatagrid(bool allow = true)
        {
            gridView.AllowFiltering = allow;
            gridView.AllowSorting = allow;
        }
    }
}
