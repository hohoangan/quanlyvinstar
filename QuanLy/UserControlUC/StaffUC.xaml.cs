﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using QuanLy.Models;
using System.Linq;
using Syncfusion.UI.Xaml.Grid;
using System.Collections.ObjectModel;
using System.Data;
using QuanLy.ModelViews;
using Microsoft.Extensions.DependencyInjection;
using Syncfusion.Data.Extensions;
using System.Windows.Media.Animation;
using System.ComponentModel;
using Syncfusion.UI.Xaml.Grid.Converter;
using Microsoft.Win32;
using System.Diagnostics;
using Syncfusion.XlsIO;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for StaffUC.xaml
    /// </summary>
    public partial class StaffUC : UserControl
    {
        ViewModel viewModel;
        public StaffUC()
        {
            InitializeComponent();
            //delete cell
            this.gridView.SelectionController = new GridSelectionControllerExt(gridView);
            gridView.Height = Constant.windowHeight;
            Load();
        }

        #region GRID
        void ChangeActionOfDatagrid(bool allow = true)
        {
            gridView.AllowFiltering = allow;
            gridView.AllowSorting = allow;
        }
        #endregion

        #region EVENT
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChangeActionOfDatagrid(false);
                this.gridView.View?.AddNew();
                this.gridView.View?.CommitNew();
            }
            catch { }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Load();
        }
        
        private void btnSaveAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult rs = MessageBox.Show("Muốn lưu tất cả thay đổi", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }
                using (AppDbContext _dbContext = new AppDbContext())
                {
                    var upadte = viewModel.Staffs.Where(x => x.Id != 0);
                    var insert = viewModel.Staffs.Where(x => x.Id == 0);
                    if (upadte != null)
                    {
                        _dbContext.Staffs.UpdateRange(upadte);
                        _dbContext.SaveChanges();
                    }
                    if (insert != null)
                    {
                        //update
                        _dbContext.Staffs.AddRange(insert);
                        _dbContext.SaveChanges();
                    }
                }
                MessageBox.Show("Lưu thành công");
                Load();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void Save_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var staff = gridView.SelectedItem as Staff;
                int Id = staff?.Id ?? 0;
                if (staff == null || !CheckData(staff))
                    return;

                using (AppDbContext _dbContext = new AppDbContext())
                {
                    if (Id == 0)
                    {
                        _dbContext.Staffs.Add(staff);
                        _dbContext.SaveChanges();
                    }
                    else
                    {
                        //update
                        _dbContext.Staffs.Update(staff);
                        _dbContext.SaveChanges();
                    }
                }

                MessageBox.Show("Lưu thành công");
                ChangeActionOfDatagrid(true);
                Load();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void btnExcel_Click(object sender, RoutedEventArgs e)
        {
            XuatExcel();
        }
        void XuatExcel()
        {
            try
            {
                if (viewModel?.Staffs.Count == 0 || gridView.ItemsSource == null)
                    return;
                MessageBoxResult rs = MessageBox.Show("Chọn nơi lưu file", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }

                string filename = string.Empty;
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Excel 97-2003 Workbook|*.xls|Excel Workbook |*.xlsx";
                if (saveFileDialog.ShowDialog() == true)
                {
                    filename = saveFileDialog.FileName;
                    var options = new ExcelExportingOptions();
                    //chọn bỏ cột khi xuất
                    options.ExcludeColumns.Add("Action");
                    options.ExcelVersion = ExcelVersion.Excel97to2003;
                    options.CellsExportingEventHandler = CellExportingHandler;
                    var excelEngine = gridView.ExportToExcel(gridView.View, options);
                    var workBook = excelEngine.Excel.Workbooks[0];
                    workBook.SaveAs(filename);
                    MessageBoxResult rsOK = MessageBox.Show("Đã tạo file thành công!\nCó muốn mở file vừa xuất?", "", MessageBoxButton.OKCancel);
                    if (rsOK.Equals(MessageBoxResult.OK))
                    {
                        Process xlsProcess = new Process();
                        xlsProcess.StartInfo.FileName = filename;
                        xlsProcess.StartInfo.UseShellExecute = true;
                        xlsProcess.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        private void CellExportingHandler(object sender, GridCellExcelExportingEventArgs e)
        {
            //https://help.syncfusion.com/wpf/datagrid/export-to-excel
            // Based on the column mapping name and the cell type, we can change the cell 
            //values while exporting to excel.
            if (e.CellType == ExportCellType.RecordCell && (e.ColumnName == "Code" || e.ColumnName == "Phone"))
            {
                e.Range.Cells[0].Value = "'" + e.CellValue.ToString();
                e.Handled = true;
            }
            if (e.CellType == ExportCellType.RecordCell && e.ColumnName == "ShiftId")
            {
                e.Range.Cells[0].Value = viewModel.Shifts
                                        .Where(x => x.Id == int.Parse(e.CellValue.ToString()))
                                        .Select(x => x.Start.ToString()).FirstOrDefault();
                e.Handled = true;
            }
            if (e.CellType == ExportCellType.RecordCell && e.ColumnName == "CityId")
            {
                e.Range.Cells[0].Value = viewModel.Cities
                                        .Where(x => x.Id == int.Parse(e.CellValue.ToString()))
                                        .Select(x => x.Name).FirstOrDefault();
                e.Handled = true;
            }
            if (e.CellType == ExportCellType.RecordCell && e.ColumnName == "Sex")
            {
                if(e.CellValue.Equals(true))
                    e.Range.Cells[0].Value = "Nam";
                else
                    e.Range.Cells[0].Value = "Nữ";
                e.Handled = true;
            }
            if (e.CellType == ExportCellType.RecordCell &&( e.ColumnName == "BirthDay" || e.ColumnName == "CMNDCreatedDate"))
            {
                if (e.CellValue.Equals(new DateTime()))
                    e.Range.Cells[0].Value = "";
                else
                    e.Range.Cells[0].Value = e.CellValue.ToString();
                e.Handled = true;
            }
        }
        #endregion

        #region FUNC
        void Load()
        {
            ChangeActionOfDatagrid(true);
            try
            {
                //gridView.ClearFilters();
                viewModel = new ViewModel();
                DataContext = viewModel;
                cboTinhTrangNhanVien.ItemsSource = Constant.TinhTrangNhanVien();
                cboShift.ItemsSource = viewModel.ShiftX;
                cboCity.ItemsSource = viewModel.Cities;
                gridView.ItemsSource = viewModel.Staffs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        bool CheckData(Staff data)
        {
            using (AppDbContext _dbContext = new AppDbContext())
            {
                var checkCode = _dbContext.Staffs.Where(x => x.Code == data.Code && x.Id != data.Id).FirstOrDefault();

                string mess = string.Empty;
                bool check = true;
                if (string.Empty.Equals(data.Code))
                {
                    check = false;
                    mess = "Nhập mã NV";
                }
                else if (checkCode != null)
                {
                    check = false;
                    mess = "Mã NV bị trùng";
                }
                else if (string.Empty.Equals(data.FullName))
                {
                    check = false;
                    mess = "Nhập tên nhân viên";
                }
                else if (data.ShiftId == 0)
                {
                    check = false;
                    mess = "Chọn ca làm việc";
                }

                if (!check)
                {
                    MessageBox.Show(mess);
                }
                return check;
            }
        }
        #endregion

        private void Del_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var del = gridView.SelectedItem as Staff;
                MessageBoxResult rs = MessageBox.Show("Muốn xóa dòng này", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }
                using (AppDbContext context = new AppDbContext())
                {
                    context.Staffs.Remove(del);
                    context.SaveChanges();
                }
                MessageBox.Show("Xoá thành công");
                Load();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
    }
}
