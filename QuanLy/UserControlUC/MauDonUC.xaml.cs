﻿using Microsoft.Win32;
using QuanLy.Models;
using QuanLy.ModelViews;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Word = Microsoft.Office.Interop.Word;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for MauDonUC.xaml
    /// </summary>
    public partial class MauDonUC : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        ViewModel viewModel;
        public Staff _staff { get; set; }
        public Staff Staff
        {
            get { return _staff; }
            set
            {
                if (value != _staff)
                {
                    _staff = value;
                    OnPropertyChanged("Staff");
                }
            }
        }
        public MauDonUC()
        {
            InitializeComponent();
            viewModel = new ViewModel();
            DataContext = this;
            cboName.ItemsSource = viewModel.Staffs.OrderBy(x => x.FullName.Trim().Split(' ').Last()).ThenBy(x => x.FullName); 
        }

        private void btn_Export_Click(object sender, RoutedEventArgs e)
        {
            if(cboName.SelectedItem == null)
            {
                MessageBox.Show("Chọn tên nhân viên");
                cboName.Focus();
                return;
            }
            MessageBoxResult rs = MessageBox.Show("Chọn nơi lưu file", "", MessageBoxButton.OKCancel);
            if (rs.Equals(MessageBoxResult.Cancel))
            {
                return;
            }

            string filename = string.Empty;
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Word 97-2003 Document|*.doc|Word Document |*.docx";
            try
            {
                SetLoadingButtuon(btn_Export, true);

                string dir = Helps.GetBasePath("DonNghiPhep.doc");

                if (saveFileDialog.ShowDialog() == true)
                {
                    filename = saveFileDialog.FileName;
                    CreateWordDocument(dir, filename);
                    MessageBoxResult rsOK = MessageBox.Show("Đã tạo file thành công!\nCó muốn mở file vừa xuất?", "", MessageBoxButton.OKCancel);
                    if (rsOK.Equals(MessageBoxResult.OK))
                    {
                        Process wordProcess = new Process();
                        wordProcess.StartInfo.FileName = filename;
                        wordProcess.StartInfo.UseShellExecute = true;
                        wordProcess.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex?.InnerException?.Message ?? ex.Message);
            }
            finally
            {
                SetLoadingButtuon(btn_Export, false);
            }
        }
        void SetLoadingButtuon(Button btn, bool status = true)
        {
            Application.Current.Dispatcher.BeginInvoke(
              DispatcherPriority.Background,
              new Action(() =>
              {
                  MaterialDesignThemes.Wpf.ButtonProgressAssist.SetIsIndeterminate(btn, status);
                  MaterialDesignThemes.Wpf.ButtonProgressAssist.SetIsIndicatorVisible(btn, status);
              }
            ));

        }
        private void CreateWordDocument(object filename, object SaveAs)
        {
            Word.Application wordApp = new Word.Application();
            object missing = Missing.Value;
            Word.Document myWordDoc = null;
            try
            {
                if (File.Exists((string)filename))
                {
                    object readOnly = false;
                    object isVisible = false;
                    wordApp.Visible = false;

                    myWordDoc = wordApp.Documents.Open(ref filename, ref missing, ref readOnly,
                                            ref missing, ref missing, ref missing,
                                            ref missing, ref missing, ref missing,
                                            ref missing, ref missing, ref missing,
                                            ref missing, ref missing, ref missing, ref missing);
                    myWordDoc.Activate();

                    //find and replace
                    //nhan vien
                    Helps.FindAndReplace(wordApp, "<Staff.FullName>", cboName.Text.ToString());
                    Helps.FindAndReplace(wordApp, "<Staff.BirthDay>", txtBirthday.SelectedDate?.ToString("dd/MM/yyyy"));
                    Helps.FindAndReplace(wordApp, "<Staff.Phone>", txtPhone.Text.Trim().ToString());
                    Helps.FindAndReplace(wordApp, "<position>", txtposition.Text.Trim().ToString());
                    Helps.FindAndReplace(wordApp, "<deparment>", txtdeparment.Text.Trim().ToString());

                    Helps.FindAndReplace(wordApp, "<year>", fromDt.SelectedDate.GetValueOrDefault().Year);
                    Helps.FindAndReplace(wordApp, "<totalday>", txtTotalDate.Text.Trim());
                    string fromdate2date = fromDt.SelectedDate.GetValueOrDefault().ToString("dd/MM/yyyy");
                    if(toDt.SelectedDate!=null)
                        fromdate2date += " đến ngày " + toDt.SelectedDate.GetValueOrDefault().ToString("dd/MM/yyyy");
                    Helps.FindAndReplace(wordApp, "<fromdate>", fromdate2date);
                    Helps.FindAndReplace(wordApp, "<location>", txtLocation.Text.Trim());
                    Helps.FindAndReplace(wordApp, "<reason>", txtReason.Text.Trim());
                   
                    //ngay ky
                    string dt = DateTime.Now.ToString("dd/MM/yyyy");
                    Helps.FindAndReplace(wordApp, "<dd>", dt.Split("/")[0]);
                    Helps.FindAndReplace(wordApp, "<mm>", dt.Split("/")[1]);
                    Helps.FindAndReplace(wordApp, "<yyyy>", dt.Split("/")[2]);

                    //Save as
                    myWordDoc.SaveAs2(ref SaveAs, ref missing, ref missing, ref missing,
                                    ref missing, ref missing, ref missing,
                                    ref missing, ref missing, ref missing,
                                    ref missing, ref missing, ref missing,
                                    ref missing, ref missing, ref missing);
                }
                else
                {
                    MessageBox.Show("Không tìm thấy file mẫu!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
            finally
            {
                myWordDoc.Close();
                wordApp.Quit();
            }
        }

        private void txtName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var staff = cboName.SelectedItem as Staff;
                Staff = staff;
                if (Staff == null)
                    return;
                using (AppDbContext context = new AppDbContext())
                {
                    var Position = (from con in context.StaffContracts.Where(x => x.StaffId == staff.Id).AsEnumerable()
                                                   .OrderByDescending(x => x.FromDate).GroupBy(x => x.StaffId).Select(x => x.First())
                             join pos in viewModel.Positions on con.PositionId equals pos.Id
                             select pos).FirstOrDefault();

                    if (Position != null)
                    {
                        txtposition.Text = Position.Name.Split("|")[1].Trim() ?? "";
                        txtdeparment.Text = Position.Department.Name;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        private void txtTotalDate_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            txt.SelectionStart = txt.Text.Length;
            txt.SelectionLength = 0;
        }

        private void txtTotalDate_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                TextBox txt = sender as TextBox;
                txt.Text = txt.Text;
            }
            catch (Exception)
            {
            }
        }
        private static readonly Regex _regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
        private static bool IsTextAllowed(string text)
        {
            return !_regex.IsMatch(text);
        }
        private void PreviewTextInputHandler(Object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        // Use the DataObject.Pasting Handler  
        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }

        private void toDt_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var fr = fromDt.SelectedDate;
                var to = toDt.SelectedDate;
                if(to == null)
                {
                    txtTotalDate.Text = "1";
                }
                else if(fr != null && to!=null)
                {
                    int day = (toDt.SelectedDate.GetValueOrDefault() - fromDt.SelectedDate.GetValueOrDefault()).Days;
                    if (day < 0)
                        day = 0;
                    txtTotalDate.Text = (day+1).ToString();
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
