﻿using Microsoft.Win32;
using OfficeOpenXml;
using QuanLy.Extensions;
using QuanLy.Models;
using QuanLy.ModelViews;
using Syncfusion.Data.Extensions;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Microsoft.EntityFrameworkCore;
using System.IO;
using OfficeOpenXml;
using System.Globalization;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for TimeKeepingUC.xaml
    /// </summary>
    public partial class TimeKeepingUC : UserControl
    {
        List<FileChamCong> lstFileChamCong;
        ViewModel viewModel;
        BackgroundWorker _backgroundWorker = new BackgroundWorker();
        public TimeKeepingUC()
        {
            InitializeComponent();
            try
            {

                lstFileChamCong = new List<FileChamCong>();
                //cboMonth.Text = DateTime.Now.Month.ToString();
                ChamCongConstant.axCZKEM1 = new zkemkeeper.CZKEM();
                // Set up the SUB Background Worker Events 
                _backgroundWorker.DoWork += _backgroundWorker_DoWork;
                _backgroundWorker.RunWorkerCompleted += _backgroundWorker_RunWorkerCompleted;
                Window_SizeChanged();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Window_SizeChanged()
        {
            try
            {
                if (Constant.Permission == 1)
                    Constant.windowHeight = SystemParameters.PrimaryScreenWidth - 250;
                else
                    Constant.windowHeight = SystemParameters.PrimaryScreenWidth - 150;
                //chinh size grid
                gridView.Height = Constant.windowHeight;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region READ
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            viewModel = new ViewModel();
            lstFileChamCong = new List<FileChamCong>();
            gridView.ItemsSource = lstFileChamCong;
            string filename = string.Empty;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel Workbook |*.xlsx|Excel 97-2003 Workbook|*.xls";
            try
            {
                if (openFileDialog.ShowDialog() == true)
                {
                    filename = openFileDialog.FileName;
                    //ReadExcel2DataGrid(filename);
                    ReadXLS(filename);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex?.InnerException?.Message ?? ex.Message);
            }
            finally { }
        }
        void ReadExcel2DataGrid(string filename)
        {
            try
            {
                DataTable dt = Excel.ReadExcel2DatatTable(filename, 2, 8);//row: 3: col:9
                foreach (DataRow item in dt.Rows)
                {
                    if (item[4].Equals(string.Empty))
                        continue;
                    DateTime days = DateTime.FromOADate(double.Parse(item[4].ToString()));
                    lstFileChamCong.Add(new FileChamCong
                    {
                        Code = item[1].ToString(),
                        FullName = item[2].ToString(),
                        Date = days,
                        Start = Excel.ConvertTimeSpanFromString(item[6].ToString()),
                        End = Excel.ConvertTimeSpanFromString(item[7].ToString()),
                        DayOfWeek = days.DayOfWeek
                    });
                }

                using (AppDbContext context = new AppDbContext())
                {
                    var data = from cong in lstFileChamCong
                               join nv in context.Staffs.Where(x => x.Status != "Đã nghỉ") on cong.Code equals nv.Code
                               join ca in context.Shifts on nv.ShiftId equals ca.Id
                               join mi in context.Missions on new { nv.Id, cong.Date } equals new { Id = mi.StaffId, Date = mi.FromDate } into missG
                               from miss in missG.DefaultIfEmpty()
                               select new FileChamCong
                               {
                                   Code = nv.Code,
                                   FullName = nv.FullName,
                                   Date = cong.Date,
                                   Start = cong.Start,
                                   End = cong.End,
                                   DayOfWeek = cong.DayOfWeek,
                                   Late = CheckLate(cong, ca, miss, nv),
                                   Early = CheckEarly(cong, ca, miss, nv),
                                   Des = CheckTimeDown(miss, nv, cong.Date)
                               };

                    lstFileChamCong = data.ToList();
                    gridView.ItemsSource = data.ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex?.InnerException?.Message ?? ex.Message);
            }
        }

        public List<DateTime> GetAllDaysInMonth(int year, int month)
        {
            List<DateTime> daysInMonth = new List<DateTime>();
            int days = DateTime.DaysInMonth(year, month);

            for (int day = 1; day <= days; day++)
            {
                DateTime date = new DateTime(year, month, day);
                daysInMonth.Add(date);
            }

            return daysInMonth;
        }
        void ReadXLS(string filePath)
        {
            lstFileChamCong = new List<FileChamCong>();
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            // Mở file excel
            using (ExcelPackage excelPackage = new ExcelPackage(new FileInfo(filePath)))
            {
                // Lấy sheet đầu tiên của file excel
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets[0];
                List<string> err = new List<string>();
                // Lấy số dòng và số cột của sheet
                int rowCount = worksheet.Dimension.Rows;
                int colCount = worksheet.Dimension.Columns;

                // Duyệt qua từng dòng của sheet
                int row = 2;
                for (; row <= rowCount; row++)
                {
                    try
                    {
                        // Đọc giá trị từng ô của dòng
                        string maNhanVien = worksheet.Cells[row, 1].Value?.ToString() ?? "";
                        if (string.IsNullOrEmpty(maNhanVien))
                            break;
                        string tenNhanVien = worksheet.Cells[row, 2].Value?.ToString() ?? "";

                        string ngayThangString = worksheet.Cells[row, 3].Value?.ToString() ?? ""; // Lấy chuỗi ngày tháng từ ô của file Excel
                        DateTime ngayThang = new DateTime();
                        if (!string.IsNullOrEmpty(ngayThangString))
                        {
                            ngayThang = DateTime.ParseExact(ngayThangString.Substring(0, 10), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }
                        string thoiGianVaoString = worksheet.Cells[row, 4].Value?.ToString() ?? ""; // Lấy chuỗi thời gian vào từ ô của file Excel
                        TimeSpan thoiGianVao = new TimeSpan();
                        if (!string.IsNullOrEmpty(thoiGianVaoString))
                        {
                            DateTime dateTimeVao = DateTime.ParseExact(thoiGianVaoString, "HH:mm", CultureInfo.InvariantCulture);
                            thoiGianVao = dateTimeVao.TimeOfDay;
                        }
                        string thoiGianRaString = worksheet.Cells[row, 5].Value?.ToString() ?? ""; // Lấy chuỗi thời gian ra từ ô của file Excel
                        TimeSpan thoiGianRa = new TimeSpan();
                        if (!string.IsNullOrEmpty(thoiGianRaString))
                        {
                            DateTime dateTimeRa = DateTime.ParseExact(thoiGianRaString, "HH:mm", CultureInfo.InvariantCulture);
                            thoiGianRa = dateTimeRa.TimeOfDay;
                        }
                        var cc = new FileChamCong
                        {
                            Code = maNhanVien,
                            FullName = tenNhanVien,
                            Date = ngayThang,
                            Start = thoiGianVao,
                            End = thoiGianRa,
                            DayOfWeek = ngayThang.DayOfWeek,
                            Des = ""
                        };
                        lstFileChamCong.Add(cc);
                        // Xử lý dữ liệu đã đọc được từ file excel ở đây
                    }
                    catch (Exception)
                    {
                        err.Add(row.ToString());
                    }
                }
                if (err.Count > 0)
                {
                    string er = "Những dòng excel bị lỗi đọc dữ liệu: ";
                    for (int i = 0; i < err.Count; i++)
                    {
                        er += string.Format("{0},", err[i]);
                    }
                    MessageBox.Show(er);
                    return;
                }
                using (AppDbContext context = new AppDbContext())
                {
                    var data = from cong in lstFileChamCong
                               join nv in context.Staffs.Where(x => x.Status != "Đã nghỉ") on cong.Code equals nv.Code
                               join ca in context.Shifts on nv.ShiftId equals ca.Id
                               join mi in context.Missions on new { nv.Id, cong.Date } equals new { Id = mi.StaffId, Date = mi.FromDate } into missG
                               from miss in missG.DefaultIfEmpty()
                               select new FileChamCong
                               {
                                   Code = nv.Code,
                                   FullName = nv.FullName,
                                   Date = cong.Date,
                                   Start = cong.Start,
                                   End = cong.End,
                                   DayOfWeek = cong.DayOfWeek,
                                   Late = CheckLate(cong, ca, miss, nv),
                                   Early = CheckEarly(cong, ca, miss, nv),
                                   Des = CheckTimeDown(miss, nv, cong.Date)
                               };

                    lstFileChamCong = data.ToList();
                    gridView.ItemsSource = data.ToList();
                }
            }
        }
        string CheckTimeDown(Mission miss, Staff nv, DateTime Date)
        {
            try
            {
                var down = viewModel.DownTimes.Where(x => x.StaffId == nv.Id)
                                              .Where(x => x.FromDate <= Date && Date <= x.ToDate).FirstOrDefault();
                string ss = "";
                if (down != null)
                    ss += string.Format("{0}: {1}", down.Reason, down.Time);

                ss += miss != null ? string.Format(" | {0} {1}: {2}", miss.Reason, miss.Time, miss.TotalHours) : "";
                return ss;
            }
            catch (Exception)
            {
                return "";
            }
        }
        TimeSpan CheckLate(FileChamCong cong, Shift ca, Mission miss, Staff nv)
        {
            //neu la ngay le thi bo qua
            var holiday = viewModel.Holidays.Where(x => x.Date.Date.Equals(cong.Date.Date)).FirstOrDefault();
            if (holiday != null)
                return new TimeSpan();
            if (cong.DayOfWeek.Equals(DayOfWeek.Sunday) || (cong.DayOfWeek.Equals(DayOfWeek.Saturday) && ca.LamThu7 == 0))
                return new TimeSpan();

            //check downtime
            var down = viewModel.DownTimes.Where(x => x.StaffId == nv.Id && !x.Time.Equals("Chiều"))
                                          .Where(x => x.FromDate <= cong.Date && cong.Date <= x.ToDate).FirstOrDefault();
            if (down != null)
                return new TimeSpan();

            //cong tac
            TimeSpan congtac = (!miss?.Time.Equals("Chiều") ?? false)
                                ? (miss.TotalHours > new TimeSpan(4, 0, 0) ? new TimeSpan(4, 0, 0) : miss.TotalHours)
                                : new TimeSpan();

            //neu thoi gian cong tac >=4h thi cho phep nghi luon
            if (congtac >= new TimeSpan(4, 0, 0))
                return new TimeSpan();

            //miss punch
            if (cong.Start.Equals(new TimeSpan()))
                return new TimeSpan(4, 0, 0);

            TimeSpan tresom = ca.Start < cong.Start ? cong.Start - ca.Start : new TimeSpan();
            tresom = tresom <= new TimeSpan(0, 0, 0) ? new TimeSpan() : tresom - new TimeSpan(0, 0, 0);
            TimeSpan kq = tresom - congtac;

            if (kq > new TimeSpan(4, 0, 0))
                return new TimeSpan(4, 0, 0);
            else if (kq < new TimeSpan())
                return new TimeSpan();
            return kq;
        }
        TimeSpan CheckEarly(FileChamCong cong, Shift ca, Mission miss, Staff nv)
        {
            //neu la ngay le thi bo qua
            var holiday = viewModel.Holidays.Where(x => x.Date.Date.Equals(cong.Date.Date)).FirstOrDefault();
            if (holiday != null)
                return new TimeSpan();

            TimeSpan End = ca.End;
            if (cong.DayOfWeek.Equals(DayOfWeek.Sunday) || (cong.DayOfWeek.Equals(DayOfWeek.Saturday) && ca.LamThu7 == 0))
                return new TimeSpan();

            //check downtime
            var down = viewModel.DownTimes.Where(x => x.StaffId == nv.Id && !x.Time.Equals("Sáng"))
                                          .Where(x => x.FromDate <= cong.Date && cong.Date <= x.ToDate).FirstOrDefault();
            if (down != null)
                return new TimeSpan();

            if (cong.DayOfWeek.Equals(DayOfWeek.Saturday))
            {
                //neu ca tu 8h thi tinh gio ra la 12AM
                //neu ca tu 9h thi tinh gio ra la 13PM
                End = ca.Sat;
                if (cong.Start.Equals(new TimeSpan()))
                    return new TimeSpan();
            }
            //cong tac
            TimeSpan congtac = (!miss?.Time.Equals("Sáng") ?? false)
                                ? (miss.TotalHours > new TimeSpan(4, 0, 0) ? new TimeSpan(4, 0, 0) : miss.TotalHours)
                                : new TimeSpan();
            //neu ngay cong tac >=4h thi cho phep nghi luon
            if (congtac >= new TimeSpan(4, 0, 0))
                return new TimeSpan();

            //miss punch
            if (cong.End.Equals(new TimeSpan()))
                return new TimeSpan(4, 0, 0);

            TimeSpan tresom = End > cong.End ? End - cong.End : new TimeSpan();
            TimeSpan kq = tresom - congtac;

            if (kq > new TimeSpan(4, 0, 0))
                return new TimeSpan(4, 0, 0);
            else if (kq < new TimeSpan())
                return new TimeSpan();
            return kq;
        }
        #endregion
        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string content = System.IO.File.ReadAllText(@"permission.txt");
                var arr = content.Split("\r\n");
                Dictionary<string, string> dir = new Dictionary<string, string>();
                arr?.ForEach(item =>
                {
                    var v = item.Split(":");
                    dir.Add(v[0], v[1]);
                });
                string chamcongtudong = dir.GetValueOrDefault("CHECKINOUT");
                if (chamcongtudong != "1")
                {
                    XuatExcel();
                }
                else
                {
                    List<string> staffList = dir.GetValueOrDefault("STAFF").Split(",").ToList();
                    int checkinHour = int.Parse(dir.GetValueOrDefault("TIMEIN"));
                    int checkoutHour = int.Parse(dir.GetValueOrDefault("TIMEOUT"));
                    int checkMinute = int.Parse(dir.GetValueOrDefault("TIMEMINUTE"));
                    XuatChamCongExcel(staffList, checkinHour, checkoutHour, checkMinute);
                }
            }
            catch (Exception)
            {
            }
        }
        void XuatExcel()
        {
            try
            {
                if (gridView.ItemsSource == null)
                    return;
                MessageBoxResult rs = MessageBox.Show("Chọn nơi lưu file", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }

                string filename = string.Empty;
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Excel 97-2003 Workbook|*.xls|Excel Workbook|*.xlsx";
                if (saveFileDialog.ShowDialog() == true)
                {
                    filename = saveFileDialog.FileName;

                    string fileXLSTemplate = Helps.GetBasePath("ChamCong.xls");
                    //Create an instance of ExcelEngine
                    using (ExcelEngine excelEngine = new ExcelEngine())
                    {
                        //Instantiate the Excel application object
                        IApplication application = excelEngine.Excel;

                        //Set the default application version
                        application.DefaultVersion = ExcelVersion.Excel2016;

                        //Load the existing Excel workbook into IWorkbook
                        IWorkbook workbook = application.Workbooks.Open(fileXLSTemplate);

                        //Get the first worksheet in the workbook into IWorksheet
                        IWorksheet worksheet = workbook.Worksheets[0];
                        IWorksheet worksheetTotal = workbook.Worksheets[1];

                        //In list cham cong
                        for (int i = 0; i < lstFileChamCong.Count(); i++)
                        {
                            FileChamCong data = lstFileChamCong[i];
                            string row = (i + 2).ToString();
                            //Assign some text in a cell
                            worksheet.Range["A" + row].Value = (i + 1).ToString();
                            worksheet.Range["B" + row].Text = data.Code.ToString();
                            worksheet.Range["C" + row].Text = data.FullName.ToString();
                            worksheet.Range["D" + row].DateTime = data.Date;
                            worksheet.Range["E" + row].Value = data.DayOfWeek.ToString();
                            worksheet.Range["F" + row].Value = data.Start.ToString();
                            worksheet.Range["G" + row].Value = data.End.ToString();
                            worksheet.Range["H" + row].Value = data.Late.ToString();
                            worksheet.Range["I" + row].Value = data.Early.ToString();
                            worksheet.Range["J" + row].Value = data.Des.ToString();

                            worksheet.Range["F" + row].NumberFormat = "hh:mm";
                            worksheet.Range["G" + row].NumberFormat = "hh:mm";
                            worksheet.Range["H" + row].NumberFormat = "hh:mm";
                            worksheet.Range["I" + row].NumberFormat = "hh:mm";

                            if (data.Early.Hours > 0 || data.Early.Minutes > 30)
                            {
                                worksheet.Range["I" + row].CellStyle.Color = System.Drawing.Color.FromName("Red");
                                worksheet.Range["I" + row].CellStyle.Font.RGBColor = System.Drawing.Color.FromName("Yellow");
                            }
                            if (data.Late.Hours > 0 || data.Late.Minutes > 30)
                            {
                                worksheet.Range["H" + row].CellStyle.Color = System.Drawing.Color.FromName("Red");
                                worksheet.Range["H" + row].CellStyle.Font.RGBColor = System.Drawing.Color.FromName("Yellow");
                            }
                        }

                        var lstTotal = lstFileChamCong.GroupBy(x => new { x.Code, x.FullName })
                                                      .OrderBy(x => x.Key.Code)
                                                      .Select(x =>
                                                      new
                                                      {
                                                          Code = x.Key.Code,
                                                          FullName = x.Key.FullName,
                                                          Late = x.Sum(c => c.Late.TotalMinutes),
                                                          Early = x.Sum(c => c.Early.TotalMinutes)
                                                      });

                        var lstOffsetDay = from nv in viewModel.Staffs
                                           join ca in viewModel.Shifts on nv.ShiftId equals ca.Id
                                           join off in viewModel.OffsetDays on nv.Id equals off.StaffId
                                           join cong in lstFileChamCong on new { nv.Code, DayOfWeek = off.Day } equals new { cong.Code, cong.DayOfWeek }
                                           group new { nv, cong, ca } by nv into g
                                           select new
                                           {
                                               Code = g.Key.Code,
                                               Minutes = Math.Round(g.Sum(x => x.cong.End > x.ca.End ? (x.cong.End - x.ca.End).TotalMinutes : 0))
                                           };

                        DateTime dt = lstFileChamCong.Max(x => x.Date);
                        var tongNghiPhep = viewModel.DownTimes.Where(x => x.FromDate.ToString("yyyyMM") == dt.ToString("yyyyMM"))
                                                    .Where(x => x.MissPunch == false).GroupBy(x => new { x.StaffId })
                                                    .Select(x => new
                                                    {
                                                        StaffId = x.Key.StaffId,
                                                        TotalDays = x.Sum(c => c.TotalDay)
                                                    });

                        var tongNghi0Phep = viewModel.DownTimes.Where(x => x.FromDate.ToString("yyyyMM") == dt.ToString("yyyyMM"))
                                                    .Where(x => x.MissPunch == true).GroupBy(x => new { x.StaffId })
                                                    .Select(x => new
                                                    {
                                                        StaffId = x.Key.StaffId,
                                                        TotalDays = x.Sum(c => c.TotalDay)
                                                    });
                        var listFinal = from nv in viewModel.Staffs.Where(x => x.Status != "Đã nghỉ")
                                        join cong in lstTotal on nv.Code equals cong.Code
                                        join off in lstOffsetDay on nv.Code equals off.Code into offG
                                        from off in offG.DefaultIfEmpty()
                                        join cophep in tongNghiPhep on nv.Id equals cophep.StaffId into cophepG
                                        from cophep in cophepG.DefaultIfEmpty()
                                        join kophep in tongNghi0Phep on nv.Id equals kophep.StaffId into kophepG
                                        from kophep in kophepG.DefaultIfEmpty()
                                        select new
                                        {
                                            Code = cong.Code,
                                            FullName = cong.FullName,
                                            Late = cong.Late,
                                            Early = cong.Early,
                                            Offset = off != null ? off.Minutes : 0,
                                            Phep = cophep != null ? cophep.TotalDays.ToString() : "",
                                            KoPhep = kophep != null ? kophep.TotalDays.ToString() : ""
                                        };

                        int dem = 0;
                        foreach (var item in listFinal)
                        {
                            string row = (dem + 3).ToString();
                            worksheetTotal.Range["A" + row].Value = (dem + 1).ToString();
                            worksheetTotal.Range["B" + row].Text = item.Code.ToString();
                            worksheetTotal.Range["C" + row].Text = item.FullName.ToString();
                            double min = (item.Late + item.Early);
                            //if (min > 390)//360 phut min > item.Offset
                            //    min = min - 390;
                            //else
                            //    min = 0;
                            double hours = (double)(min / (60));
                            worksheetTotal.Range["D" + row].Value = Math.Round(hours, 1).ToString();
                            worksheetTotal.Range["E" + row].Value = item.Phep.ToString();
                            worksheetTotal.Range["F" + row].Value = item.KoPhep.ToString();
                            worksheetTotal.Range["G" + row].Value = item.Offset.ToString();

                            if (min > 260)
                            {
                                worksheetTotal.Range["D" + row].CellStyle.Color = System.Drawing.Color.FromName("Red");
                                worksheetTotal.Range["D" + row].CellStyle.Font.RGBColor = System.Drawing.Color.FromName("Yellow");
                            }
                            dem++;
                        }
                        //Save the Excel document
                        workbook.SaveAs(filename);
                    }

                    MessageBoxResult rsOK = MessageBox.Show("Đã tạo file thành công!\nCó muốn mở file vừa xuất?", "", MessageBoxButton.OKCancel);
                    if (rsOK.Equals(MessageBoxResult.OK))
                    {
                        Process xlsProcess = new Process();
                        xlsProcess.StartInfo.FileName = filename;
                        xlsProcess.StartInfo.UseShellExecute = true;
                        xlsProcess.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            //nghỉ phép
            try
            {
                MenuItem menuItem = sender as MenuItem;
                string header = menuItem.Header.ToString();
                bool miss = header.ToLower().Contains("không") ? true : false;
                string time = header.Split(":")[1].Trim();
                double totalDay = header.ToLower().Contains("cả") ? 1 : 0.5;
                var row = gridView.SelectedItem as FileChamCong;
                if (row == null)
                    return;
                var staff = viewModel.Staffs.Where(x => x.Code.Equals(row.Code)).FirstOrDefault();
                using (AppDbContext _dbContext = new AppDbContext())
                {
                    var phep = new DownTime
                    {
                        Des = "Thêm tự động từ tab chấm công",
                        FromDate = row.Date,
                        ToDate = row.Date,
                        MissPunch = miss,
                        Reason = miss ? "Không Phép" : "Có phép",
                        StaffId = staff.Id,
                        Time = time,
                        Location = "Hồ Chí Minh",
                        TotalDay = totalDay
                    };
                    _dbContext.DownTimes.Add(phep);
                    _dbContext.SaveChanges();
                    MessageBox.Show("Thêm thành công");

                    //update grid
                    switch (time)
                    {
                        case "Sáng": (gridView.SelectedItem as FileChamCong).Late = new TimeSpan(); break;
                        case "Chiều": (gridView.SelectedItem as FileChamCong).Early = new TimeSpan(); break;
                        default:
                            (gridView.SelectedItem as FileChamCong).Late = new TimeSpan();
                            (gridView.SelectedItem as FileChamCong).Early = new TimeSpan();
                            break;
                    }
                    gridView.View.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            //quên chấm công
            try
            {
                MenuItem menuItem = sender as MenuItem;
                string header = menuItem.Header.ToString();
                string time = header.Split(":")[1].Trim();
                TimeSpan totalHours = header.ToLower().Contains("cả") ? new TimeSpan(8, 0, 0) : new TimeSpan(4, 0, 0);
                var row = gridView.SelectedItem as FileChamCong;
                if (row == null)
                    return;
                var staff = viewModel.Staffs.Where(x => x.Code.Equals(row.Code)).FirstOrDefault();
                using (AppDbContext _dbContext = new AppDbContext())
                {
                    var phep = new Mission
                    {
                        Des = "Thêm tự động từ tab chấm công",
                        FromDate = row.Date,
                        Reason = "Quên chấm công",
                        StaffId = staff.Id,
                        Time = time,
                        Location = "Hồ Chí Minh",
                        TotalHours = totalHours
                    };
                    _dbContext.Missions.Add(phep);
                    _dbContext.SaveChanges();
                    MessageBox.Show("Thêm thành công");

                    //update grid
                    switch (time)
                    {
                        case "Sáng": (gridView.SelectedItem as FileChamCong).Late = new TimeSpan(); break;
                        case "Chiều": (gridView.SelectedItem as FileChamCong).Early = new TimeSpan(); break;
                        default:
                            (gridView.SelectedItem as FileChamCong).Late = new TimeSpan();
                            (gridView.SelectedItem as FileChamCong).Early = new TimeSpan();
                            break;
                    }
                    gridView.View.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            //công tác
            try
            {
                MenuItem menuItem = sender as MenuItem;
                string header = menuItem.Header.ToString();
                string time = header.Split(":")[1].Trim();
                TimeSpan totalHours = header.ToLower().Contains("cả") ? new TimeSpan(8, 0, 0) : new TimeSpan(4, 0, 0);
                var row = gridView.SelectedItem as FileChamCong;
                if (row == null)
                    return;
                var staff = viewModel.Staffs.Where(x => x.Code.Equals(row.Code)).FirstOrDefault();

                var phep = new Mission
                {
                    Des = "Thêm tự động từ tab chấm công",
                    FromDate = row.Date,
                    Reason = "Đi công tác",
                    StaffId = staff.Id,
                    Time = time,
                    Location = "Hồ Chí Minh",
                };

                PopupAddMission pop = new PopupAddMission(phep);
                pop.passControl = new PopupAddMission.PassControl(PassData);
                pop.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void PassData(object sender)
        {
            try
            {
                var row = (gridView.SelectedItem as FileChamCong);
                Mission mission = ((Mission)sender);
                // Set de text of the textbox to the value of the textbox of form 2
                TimeSpan totalHours = mission.TotalHours;
                //update grid
                if (totalHours != null)
                {
                    switch (mission.Time)
                    {
                        case "Sáng": row.Late = (row.Late > totalHours) ? row.Late - totalHours : new TimeSpan(); break;
                        case "Chiều": row.Early = (row.Early > totalHours) ? row.Early - totalHours : new TimeSpan(); break;
                        default:
                            (gridView.SelectedItem as FileChamCong).Late = new TimeSpan();
                            (gridView.SelectedItem as FileChamCong).Early = new TimeSpan();
                            break;
                    }
                }
                gridView.View.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                viewModel = new ViewModel();
                lstFileChamCong = new List<FileChamCong>();
                gridView.ItemsSource = lstFileChamCong;

                if (_backgroundWorker.IsBusy)
                    return;
                //SetLoadingButtuon(btnLoad, true);
                // Run the Background Worker
                _backgroundWorker.RunWorkerAsync(5000);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #region MÁY CHAM CONG
        void LoadDataFromMCC()
        {
            try
            {
                //ChamCongConstant.bIsConnected = false;
                if (ChamCongConstant.bIsConnected == false)
                {
                    //ChamCongConstant.axCZKEM1 = new zkemkeeper.CZKEM();
                    ChamCongConstant.bIsConnected = ChamCongConstant.axCZKEM1.Connect_Net(ChamCongConstant.IPmcc, ChamCongConstant.Port);
                }
                if (!ChamCongConstant.bIsConnected)
                {
                    Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    new Action(() =>
                    {
                        MessageBox.Show("Không thể kết nối MCC, kiểm tra lại mạng internet");
                    }
                    ));
                    return;
                }

                ZkemkeeperAPI zkem = new ZkemkeeperAPI();
                lstFileChamCong = zkem.lstFileChamCong;
                if (lstFileChamCong == null)
                {
                    Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    new Action(() =>
                    {
                        MessageBox.Show("Không lấy được data");
                    }
                    ));
                    return;
                }

                this.Dispatcher.Invoke(() =>
                {
                    Random r = new Random();
                    using (AppDbContext context = new AppDbContext())
                    {
                        var data = from cong in lstFileChamCong
                                   join nv in context.Staffs.Where(x => x.Status != "Đã nghỉ") on cong.Code equals nv.Code
                                   join ca in context.Shifts on nv.ShiftId equals ca.Id
                                   join mi in context.Missions on new { nv.Id, cong.Date } equals new { Id = mi.StaffId, Date = mi.FromDate } into missG
                                   from miss in missG.DefaultIfEmpty()
                                   select new FileChamCong
                                   {
                                       Code = nv.Code,
                                       FullName = nv.FullName,
                                       Date = cong.Date,
                                       Start = cong.Start,
                                       //End = "5,9,12,14,19,22,25,28".Contains(cong.Date.Day.ToString()) ? new TimeSpan(17, r.Next(30, 40), r.Next(0, 59)) : cong.End,
                                       End = cong.End,
                                       DayOfWeek = cong.DayOfWeek,
                                       //tinh tre/som
                                       /*
                                        * neu co phep ngay do bsang/chieu/ca ngay thi cộng vô giờ trễ sớm luôn
                                        */
                                       Late = ChamCongConstant.CheckLate(viewModel, cong, ca, miss, nv),
                                       Early = new TimeSpan(),//ChamCongConstant.CheckEarly(viewModel, cong, ca, miss, nv),
                                       Des = ChamCongConstant.CheckTimeDown(viewModel, miss, nv, cong.Date)
                                   };
                        if (lstFileChamCong != null)
                        {
                            lstFileChamCong = data.ToList();
                            gridView.ItemsSource = lstFileChamCong;
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void _backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Do something 
            LoadDataFromMCC();
        }
        // Completed Method 
        void _backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("Cancelled");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("Exception Thrown");
            }
            else
            {
                if (ChamCongConstant.bIsConnected)
                {
                    ChamCongConstant.axCZKEM1.Disconnect();
                    ChamCongConstant.bIsConnected = false;
                }
            }
            //SetLoadingButtuon(btnLoad, false);
        }
        void SetLoadingButtuon(Button btn, bool status = true)
        {
            Application.Current.Dispatcher.BeginInvoke(
              DispatcherPriority.Background,
              new Action(() =>
              {
                  MaterialDesignThemes.Wpf.ButtonProgressAssist.SetIsIndeterminate(btn, status);
                  MaterialDesignThemes.Wpf.ButtonProgressAssist.SetIsIndicatorVisible(btn, status);
              }
            ));

        }
        #endregion

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ComboBox cbo = (sender as ComboBox);
                ComboBoxItem item = cbo.SelectedItem as ComboBoxItem;
                int month = int.Parse(item.Content.ToString());
                ChamCongConstant.month = month;
                if (DateTime.Now.Month == 1 && month == 12)
                {
                    ChamCongConstant.year = DateTime.Now.Year - 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        #region xuat chấm công
        void XuatChamCongExcel(List<string> staffList, int checkinHour = 8, int checkoutHour = 18, int checkMinute = 0)
        {
            try
            {
                if (staffList.Count <= 0)
                    return;
                MessageBoxResult rs = MessageBox.Show("Chọn nơi lưu file", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }

                string filename = string.Empty;
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Excel 97-2003 Workbook|*.xls|Excel Workbook|*.xlsx";
                if (saveFileDialog.ShowDialog() == true)
                {
                    filename = saveFileDialog.FileName;

                    string fileXLSTemplate = Helps.GetBasePath("Data.xls");
                    //Create an instance of ExcelEngine
                    using (ExcelEngine excelEngine = new ExcelEngine())
                    {
                        //Instantiate the Excel application object
                        IApplication application = excelEngine.Excel;

                        //Set the default application version
                        application.DefaultVersion = ExcelVersion.Excel2016;

                        //Load the existing Excel workbook into IWorkbook
                        IWorkbook workbook = application.Workbooks.Open(fileXLSTemplate);

                        //Get the first worksheet in the workbook into IWorksheet
                        IWorksheet worksheet = workbook.Worksheets[0];

                        //In list cham cong
                        //for integers
                        Random r = new Random();
                        int row = 1;
                        for (int i = 0; i < staffList.Count(); i++)
                        {
                            DateTime fromdate = new DateTime(ChamCongConstant.year, ChamCongConstant.month, 01);
                            DateTime todate = fromdate.AddMonths(1).AddDays(-1);
                            for (var j = fromdate.Date; j.Date <= todate.Date; j = j.AddDays(1))
                            {
                                var DayOfW = j.DayOfWeek;
                                if (DayOfW == DayOfWeek.Sunday)
                                    continue;
                                row++;
                                TimeSpan checkin = new TimeSpan(checkinHour - 1, r.Next(40, 59), r.Next(0, 59));
                                if (checkMinute == 30)//neu khung gio la 8:30 -> 15h30
                                {
                                    checkin = new TimeSpan(checkinHour, r.Next(15, 29), r.Next(0, 59));
                                }
                                j = j.Date.Add(checkin);
                                ///CHECKIN
                                worksheet.Range["A" + row].Text = staffList[i];
                                worksheet.Range["B" + row].Value = int.Parse(staffList[i].ToString()).ToString();
                                worksheet.Range["C" + row].DateTime = j.Date;
                                worksheet.Range["D" + row].DateTime = j;
                                worksheet.Range["E" + row].Value = "I";
                                worksheet.Range["F" + row].Value = "FP";
                                worksheet.Range["G" + row].Value = "1";
                                worksheet.Range["H" + row].Text = "00000000000";
                                ///CHECKOUT
                                row++;
                                TimeSpan checkout = new TimeSpan(checkoutHour, r.Next(1, 5), r.Next(0, 59));
                                if (checkMinute == 30)//neu khung gio la 8:30 -> 15h30
                                {
                                    checkout = new TimeSpan(checkoutHour, r.Next(31, 45), r.Next(0, 59));
                                }
                                if (DayOfW == DayOfWeek.Saturday)
                                    checkout = new TimeSpan(12, r.Next(2, 17), r.Next(0, 59)); ;
                                j = j.Date.Add(checkout);
                                worksheet.Range["A" + row].Text = staffList[i];
                                worksheet.Range["B" + row].Value = int.Parse(staffList[i].ToString()).ToString();
                                worksheet.Range["C" + row].DateTime = j.Date;
                                worksheet.Range["D" + row].DateTime = j;
                                worksheet.Range["E" + row].Value = "O";
                                worksheet.Range["F" + row].Value = "PC";
                                worksheet.Range["G" + row].Value = "2";
                                worksheet.Range["H" + row].Text = "00000000000";
                                //worksheet.Range["I" + row].NumberFormat = "hh:mm";
                            }
                        }
                        //Save the Excel document
                        workbook.SaveAs(filename);
                    }

                    MessageBoxResult rsOK = MessageBox.Show("Đã tạo file thành công!\nCó muốn mở file vừa xuất?", "", MessageBoxButton.OKCancel);
                    if (rsOK.Equals(MessageBoxResult.OK))
                    {
                        Process xlsProcess = new Process();
                        xlsProcess.StartInfo.FileName = filename;
                        xlsProcess.StartInfo.UseShellExecute = true;
                        xlsProcess.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        void XuatBangCong()
        {
            try
            {
                MessageBoxResult rs = MessageBox.Show("Chọn nơi lưu file", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }

                string filename = string.Empty;
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Excel 97-2003 Workbook|*.xls|Excel Workbook|*.xlsx";
                if (saveFileDialog.ShowDialog() == true)
                {
                    filename = saveFileDialog.FileName;

                    string fileXLSTemplate = Helps.GetBasePath("MauBCC.xls");
                    //Create an instance of ExcelEngine
                    using (ExcelEngine excelEngine = new ExcelEngine())
                    {
                        //Instantiate the Excel application object
                        IApplication application = excelEngine.Excel;

                        //Set the default application version
                        application.DefaultVersion = ExcelVersion.Excel2016;

                        //Load the existing Excel workbook into IWorkbook
                        IWorkbook workbook = application.Workbooks.Open(fileXLSTemplate);

                        //Get the first worksheet in the workbook into IWorksheet
                        IWorksheet worksheet = workbook.Worksheets[0];

                        //In list cham cong
                        //for integers
                        Random r = new Random();
                        int row = 9;//in từ dòng 9
                        int i = 1;
                        var listNV = lstFileChamCong.GroupBy(x => new { x.Code, x.FullName }).ToList();
                        DateTime firstDate = lstFileChamCong.FirstOrDefault().Date;
                        worksheet.Range["U3"].Value = firstDate.Month.ToString();
                        worksheet.Range["W3"].Value = firstDate.Year.ToString();
                        foreach (var group in listNV)
                        {
                            string maNV = group.Key.Code;
                            string ten = group.Key.FullName;
                            worksheet.Range["A" + row].Text = (i++).ToString();
                            worksheet.Range["B" + row].Text = maNV;
                            worksheet.Range["C" + row].Text = ten;
                            int col = 4;
                            foreach (FileChamCong bc in group)
                            {
                                var nv = viewModel.Staffs.Where(x => x.Code == bc.Code).FirstOrDefault();
                                var ca = viewModel.Shifts.Where(x => x.Id == nv.ShiftId).FirstOrDefault();
                                string cong = "x";
                                #region check ngay cong
                                if ((bc.DayOfWeek.Equals(DayOfWeek.Sunday)) || ( bc.DayOfWeek.Equals(DayOfWeek.Saturday) && ca.LamThu7 == 0))
                                {
                                    continue;
                                }
                                if (bc.Start != new TimeSpan(0, 0, 0) && bc.Start <= new TimeSpan(13, 0, 0) && bc.End >= new TimeSpan(13, 0, 0))
                                {
                                    cong = "x";
                                    //neu lam thu7
                                    if(bc.DayOfWeek.Equals(DayOfWeek.Saturday) && ca.LamThu7 == 2)//0: ko, 1:lam, 2: lam 1/2
                                        cong = "x/2";
                                }
                                else if (bc.Start != new TimeSpan(0, 0, 0) || bc.End != new TimeSpan(0, 0, 0))
                                    cong = "x/2";
                                //neu la ngay le thi bo qua
                                var holiday = viewModel.Holidays.Where(x => x.Date.Date.Equals(bc.Date)).FirstOrDefault();
                                if (holiday != null)
                                    cong = "NL";
                                //check downtime
                                
                                var down = viewModel.DownTimes.Where(x => x.StaffId == nv.Id)
                                                              .Where(x => x.FromDate <= bc.Date && bc.Date <= x.ToDate)
                                                              .FirstOrDefault();
                                if (down != null)
                                {
                                    if (down.Time.Equals("Cả ngày"))
                                        cong = "P";
                                    else if (down.MissPunch == false)//nghi khong phep
                                        cong = "P/2";
                                    else
                                        cong = "P/2.1";
                                }
                                //cong tac
                                var miss = viewModel.Missions.Where(x => x.StaffId == nv.Id)
                                                            .Where(x => x.FromDate <= bc.Date)
                                                            .FirstOrDefault();
                                if (miss != null)
                                {
                                    if (miss.Time.Equals("Cả ngày"))
                                        cong = "x";
                                    //neu chi co ddi lam 1/2 va co di ctac 
                                    else if (bc.Start != new TimeSpan(0, 0, 0) || bc.End != new TimeSpan(0, 0, 0))
                                        cong = "x";
                                }
                                worksheet[row, 4 + bc.Date.Day].Text = cong;
                                if (cong != "x")
                                {
                                    //worksheet[row, 4 + bc.Date.Day].CellStyle.Color = System.Drawing.Color.FromName("Red");
                                    worksheet[row, 4 + bc.Date.Day].CellStyle.Font.RGBColor = System.Drawing.Color.FromName("Red");
                                }
                                
                                #endregion
                                worksheet.Range["AJ" + row].Formula = string.Format("=COUNTIF($E{0}:$AI{1}, \"x\")", row, row);
                                worksheet.Range["AK" + row].Formula = string.Format("=COUNTIF($E{0}:$AI{1},\"X/2\")/2+COUNTIF($E{0}:$AI{1},\"P/2\")/2", row, row);
                                worksheet.Range["AL" + row].Formula = string.Format("=COUNTIF($E{0}:$AI{1}, \"TV\")", row, row);
                                worksheet.Range["AM" + row].Formula = string.Format("=COUNTIF($E{0}:$AI{1}, \"TV/2\")/2", row, row);
                                worksheet.Range["AN" + row].Formula = string.Format("=COUNTIF($E{0}:$AI{1},\"P\")+COUNTIF($E{0}:$AI{1},\"P/2\")/2+COUNTIF($E{0}:$AI{1},\"P/2.1\")/2", row, row);
                                worksheet.Range["AO" + row].Formula = string.Format("=COUNTIF($E{0}:$AI{1}, \"NL\")", row, row);
                                worksheet.Range["AP" + row].Formula = string.Format("=COUNTIF($E{0}:$AI{1}, \"NCĐ\")", row, row);
                                worksheet.Range["AQ" + row].Formula = string.Format("=COUNTIF($E{0}:$AI{1},\"KL\")+COUNTIF($E{0}:$AI{1},\"x/2\")/2", row, row);
                                worksheet.Range["AR" + row].Formula = string.Format("=SUM(AJ{0}:AP{1})", row, row);
                            }
                            row++;
                        }
                        //Style
                        // Đặt kiểu viền cho phạm vi ô này
                        IRange range = worksheet.Range["A9:AT" + row];
                        // Đặt màu viền cho phạm vi ô này
                        worksheet.Range["A9:AT" + row].Borders.Color = ExcelKnownColors.Black;
                        //Dong tong cong
                        worksheet.Range["C" + row].Text = "TỔNG CỘNG";
                        worksheet.Range["AJ" + row].Formula = string.Format("=SUBTOTAL(9,AJ9:AJ{0})", row-1);
                        worksheet.Range["AK" + row].Formula = string.Format("=SUBTOTAL(9,AK9:AK{0})", row-1);
                        worksheet.Range["AL" + row].Formula = string.Format("=SUBTOTAL(9,AL9:AL{0})", row-1);
                        worksheet.Range["AM" + row].Formula = string.Format("=SUBTOTAL(9,AM9:AM{0})", row-1);
                        worksheet.Range["AN" + row].Formula = string.Format("=SUBTOTAL(9,AN9:AN{0})", row-1);
                        worksheet.Range["AO" + row].Formula = string.Format("=SUBTOTAL(9,AO9:AO{0})", row-1);
                        worksheet.Range["AP" + row].Formula = string.Format("=SUBTOTAL(9,AP9:AP{0})", row-1);
                        worksheet.Range["AQ" + row].Formula = string.Format("=SUBTOTAL(9,AQ9:AQ{0})", row-1);
                        worksheet.Range["AR" + row].Formula = string.Format("=SUBTOTAL(9,AR9:AR{0})", row-1);
                        worksheet[string.Format("AJ{0}:AR{0}",row)].CellStyle.Color = System.Drawing.Color.FromName("Yellow");
                        worksheet[string.Format("AJ{0}:AR{0}", row)].CellStyle.Font.RGBColor = System.Drawing.Color.FromName("Red");
                        row += 1;
                        worksheet.Range["AJ" + row].Formula = string.Format("=SUM(AJ9:AJ{0})", row - 2);
                        worksheet.Range["AK" + row].Formula = string.Format("=SUM(AK9:AK{0})", row - 2);
                        worksheet.Range["AL" + row].Formula = string.Format("=SUM(AL9:AL{0})", row - 2);
                        worksheet.Range["AM" + row].Formula = string.Format("=SUM(AM9:AM{0})", row - 2);
                        worksheet.Range["AN" + row].Formula = string.Format("=SUM(AN9:AN{0})", row - 2);
                        worksheet.Range["AO" + row].Formula = string.Format("=SUM(AO9:AO{0})", row - 2);
                        worksheet.Range["AP" + row].Formula = string.Format("=SUM(AP9:AP{0})", row - 2);
                        worksheet.Range["AQ" + row].Formula = string.Format("=SUM(AQ9:AQ{0})", row - 2);
                        worksheet.Range["AR" + row].Formula = string.Format("=SUM(AR9:AR{0})", row - 2);
                        row += 1;
                        worksheet.Range["A" + row].Text = "Ký hiệu:";
                        worksheet.Range["C" + row].Text = "Đủ ngày công : X";
                        worksheet.Range["E" + row].Text = "Nghỉ phép : P";
                        worksheet.Range["Q" + row].Text = "Thai sản : TS";
                        worksheet.Range["X" + row].Text = "Nghỉ nửa ngày phép : P/2";
                        row += 1;
                        worksheet.Range["C" + row].Text = "Nửa ngày công: X/2";
                        worksheet.Range["E" + row].Text = "Nghỉ chế độ : NCĐ";
                        worksheet.Range["Q" + row].Text = "Không lương : LK";
                        worksheet.Range["X" + row].Text = "Nghỉ nửa ngày phép, nửa ngày KL: P/2.1";
                        row += 4;
                        worksheet.Range["E" + row].Text = "Người lập";
                        worksheet.Range["AF" + row].Text = "Tổng giám đốc";
                        row += 1;
                        worksheet.Range["E" + row].Text = "(Ký, họ tên)";
                        worksheet.Range["AF" + row].Text = "(Ký, đóng dấu)";
                        
                        //Save the Excel document
                        workbook.SaveAs(filename);
                    }

                    MessageBoxResult rsOK = MessageBox.Show("Đã tạo file thành công!\nCó muốn mở file vừa xuất?", "", MessageBoxButton.OKCancel);
                    if (rsOK.Equals(MessageBoxResult.OK))
                    {
                        Process xlsProcess = new Process();
                        xlsProcess.StartInfo.FileName = filename;
                        xlsProcess.StartInfo.UseShellExecute = true;
                        xlsProcess.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        private void btnExportBCC_Click(object sender, RoutedEventArgs e)
        {
            XuatBangCong();
        }
    }
}
