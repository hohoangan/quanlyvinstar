﻿using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using QuanLy.Models;
using QuanLy.ModelViews;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for TodoReportUC.xaml
    /// </summary>
    public partial class TodoReportUC : UserControl
    {
        ViewModel viewModel;
        public SeriesCollection SeriesCollection { get; set; }
        public SeriesCollection SeriesCollection2 { get; set; }
        public TodoReportUC()
        {
            InitializeComponent();
            Load();
        }
        void Load()
        {
            try
            {
                viewModel = new ViewModel();
                DataContext = viewModel;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        void LoadChart(IEnumerable<TodoList> todolst)
        {
            try
            {
                //Chart
                SeriesCollection = new SeriesCollection();

                var series = (from todo in todolst
                              join team in viewModel.Teams on todo.TeamId equals team.Id
                               join wt in viewModel.WorkTypes on todo.WorkTypeId equals wt.Id
                               group new { todo, team, wt } by new { wt } into g
                               select new
                               {
                                   TotalWork = g.Count(),
                                   WorkTypeName = g.Key.wt.Name,
                                   TotoalTime = g.Sum(x => (x.todo.End.GetValueOrDefault() - x.todo.Start).Days+1)
                               }
                              ).ToList();

                series?.ForEach(item =>
                {
                    SeriesCollection.Add(new PieSeries
                    {
                        Title = string.Format("{0} ({1} days)", item.WorkTypeName, item.TotoalTime),
                        Values = new ChartValues<ObservableValue> { new ObservableValue(item.TotalWork) },
                        DataLabels = true,
                        FontSize = 15,
                        LabelPoint = point => point.Y + "cv" //value hiển thị trong chart
                    });
                });
                Chart.Series = SeriesCollection;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        void LoadChartTeam(IEnumerable<TodoList> todolst)
        {
            try
            {
                //Chart
                SeriesCollection2 = new SeriesCollection();

                var series = (from todo in todolst
                              join team in viewModel.Teams on todo.TeamId equals team.Id
                              join wt in viewModel.WorkTypes on todo.WorkTypeId equals wt.Id
                              group new { todo, team, wt } by new { team } into g
                              select new
                              {
                                  TotalWork = g.Count(),
                                  TeamName = g.Key.team.Name,
                                  TotoalTime = g.Sum(x => (x.todo.End.GetValueOrDefault() - x.todo.Start).Days + 1)
                              }
                              ).ToList();

                series?.ForEach(item =>
                {
                    SeriesCollection2.Add(new PieSeries
                    {
                        Title = string.Format("{0} ({1} cv)", item.TeamName, item.TotalWork),
                        Values = new ChartValues<ObservableValue> { new ObservableValue(item.TotoalTime) },
                        DataLabels = true,
                        FontSize = 15,
                        LabelPoint = point => point.Y + "days" //value hiển thị trong chart
                    });
                });
                ChartTeam.Series = SeriesCollection2;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void Datetime_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var todolst = viewModel.TodoLists.Where(x => x.End != null);
                if (dateStart.SelectedDate != null)
                    todolst = todolst.Where(x => x.Start >= dateStart.SelectedDate);
                if (dateEnd.SelectedDate != null)
                    todolst = todolst.Where(x => x.End <= dateEnd.SelectedDate);

                LoadChart(todolst);
                LoadChartTeam(todolst);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
