﻿using Microsoft.Win32;
using QuanLy.ModelViews;
using Syncfusion.UI.Xaml.Grid.Converter;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for ReviewCandidateListUC.xaml
    /// </summary>
    public partial class ReviewCandidateListUC : UserControl
    {
        ViewModel viewModel;
        public ReviewCandidateListUC()
        {
            InitializeComponent();
            this.gridView.SelectionController = new GridSelectionControllerExt(gridView);
            gridView.Height = Constant.windowHeight;
            Load();
        }

        #region GRID
        void ChangeActionOfDatagrid(bool allow = true)
        {
            gridView.AllowFiltering = allow;
            gridView.AllowSorting = allow;
        }
        #endregion

        #region event
        private void btnExcel_Click(object sender, RoutedEventArgs e)
        {
            XuatExcel();
        }
        #endregion

        #region FUNC
        void Load()
        {
            ChangeActionOfDatagrid(true);
            try
            {
                //gridView.ClearFilters();
                viewModel = new ViewModel();
                DataContext = viewModel;

                gridView.ItemsSource = viewModel.CandidateProcesses.OrderByDescending(x => x.CandidateStepId).GroupBy(x=>x.CandidateId).Select(x=>x.First());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        void XuatExcel()
        {
            try
            {
                if (viewModel?.CandidateProcesses.Count == 0 || gridView.ItemsSource == null)
                    return;
                MessageBoxResult rs = MessageBox.Show("Chọn nơi lưu file", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }

                string filename = string.Empty;
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Excel 97-2003 Workbook|*.xls|Excel Workbook |*.xlsx";
                if (saveFileDialog.ShowDialog() == true)
                {
                    filename = saveFileDialog.FileName;
                    var options = new ExcelExportingOptions();
                    //chọn bỏ cột khi xuất
                    options.ExcludeColumns.Add("Save");
                    options.ExcludeColumns.Add("Del");
                    options.ExcelVersion = ExcelVersion.Excel97to2003;
                    options.CellsExportingEventHandler = CellExportingHandler;
                    var excelEngine = gridView.ExportToExcel(gridView.View, options);
                    var workBook = excelEngine.Excel.Workbooks[0];
                    workBook.SaveAs(filename);
                    MessageBoxResult rsOK = MessageBox.Show("Đã tạo file thành công!\nCó muốn mở file vừa xuất?", "", MessageBoxButton.OKCancel);
                    if (rsOK.Equals(MessageBoxResult.OK))
                    {
                        Process xlsProcess = new Process();
                        xlsProcess.StartInfo.FileName = filename;
                        xlsProcess.StartInfo.UseShellExecute = true;
                        xlsProcess.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void CellExportingHandler(object sender, GridCellExcelExportingEventArgs e)
        {
            //https://help.syncfusion.com/wpf/datagrid/export-to-excel
            // Based on the column mapping name and the cell type, we can change the cell 
            //values while exporting to excel.
            if (e.CellType == ExportCellType.RecordCell && e.ColumnName == "CandidateId")
            {
                e.Range.Cells[0].Value = viewModel.Candidates
                                        .Where(x => x.Id == int.Parse(e.CellValue.ToString()))
                                        .Select(x => x.FullName).FirstOrDefault();
                e.Handled = true;
            }
            if (e.CellType == ExportCellType.RecordCell && e.ColumnName == "CandidateStepId")
            {
                e.Range.Cells[0].Value = viewModel.CandidateSteps
                                        .Where(x => x.Id == int.Parse(e.CellValue.ToString()))
                                        .Select(x => x.Name).FirstOrDefault();
                e.Handled = true;
            }
            if (e.CellType == ExportCellType.RecordCell && e.ColumnName == "CandidateStatusId")
            {
                e.Range.Cells[0].Value = viewModel.CandidateStatuses
                                        .Where(x => x.Id == int.Parse(e.CellValue.ToString()))
                                        .Select(x => x.Name).FirstOrDefault();
                e.Handled = true;
            }
        }
        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Load();
        }
    }
}
