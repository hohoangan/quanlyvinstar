﻿using QuanLy.Extensions;
using QuanLy.Models;
using QuanLy.ModelViews;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
//using ZKSoftwareAPI;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for PunchingMachine.xaml
    /// </summary>
    public partial class PunchingMachine : UserControl
    {
        List<FileChamCong> lstFileChamCong;
        ViewModel viewModel;
        BackgroundWorker _backgroundWorker = new BackgroundWorker();
        public PunchingMachine()
        {
            InitializeComponent();
            gridView.Height = Constant.windowHeight;
            try
            {
                ChamCongConstant.axCZKEM1 = new zkemkeeper.CZKEM();
                // Set up the SUB Background Worker Events 
                _backgroundWorker.DoWork += _backgroundWorker_DoWork;
                _backgroundWorker.RunWorkerCompleted += _backgroundWorker_RunWorkerCompleted;
            }
            catch { }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                viewModel = new ViewModel();
                lstFileChamCong = new List<FileChamCong>();
                gridView.ItemsSource = lstFileChamCong;

                if (_backgroundWorker.IsBusy)
                    return;
                SetLoadingButtuon(btnLoad, true);
                // Run the Background Worker
                _backgroundWorker.RunWorkerAsync(5000);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region BACKGROUND
        void LoadDataFromMCC()
        {
            try
            {
                //ChamCongConstant.bIsConnected = false;
                if (ChamCongConstant.bIsConnected == false)
                {
                    //ChamCongConstant.axCZKEM1 = new zkemkeeper.CZKEM();
                    ChamCongConstant.bIsConnected = ChamCongConstant.axCZKEM1.Connect_Net(ChamCongConstant.IPmcc, ChamCongConstant.Port);
                }
                if (!ChamCongConstant.bIsConnected)
                {
                    Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    new Action(() =>
                    {
                        MessageBox.Show("Không thể kết nối MCC, kiểm tra lại mạng internet");
                    }
                    ));
                    return;
                }

                ZkemkeeperAPI zkem = new ZkemkeeperAPI();
                lstFileChamCong = zkem.lstFileChamCong;
                if (lstFileChamCong == null)
                {
                    Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    new Action(() =>
                    {
                        MessageBox.Show("Không lấy được data");
                    }
                    ));
                    return;
                }

                this.Dispatcher.Invoke(() =>
                {
                    using (AppDbContext context = new AppDbContext())
                    {
                        var data = from cong in lstFileChamCong
                                   join nv in context.Staffs on cong.Code equals nv.Code
                                   join ca in context.Shifts on nv.ShiftId equals ca.Id
                                   join mi in context.Missions on new { nv.Id, cong.Date } equals new { Id = mi.StaffId, Date = mi.FromDate } into missG
                                   from miss in missG.DefaultIfEmpty()
                                   select new FileChamCong
                                   {
                                       Code = nv.Code,
                                       FullName = nv.FullName,
                                       Date = cong.Date,
                                       Start = cong.Start,
                                       End = cong.End,
                                       DayOfWeek = cong.DayOfWeek,
                                       //tinh tre/som
                                       /*
                                        * neu co phep ngay do bsang/chieu/ca ngay thi cộng vô giờ trễ sớm luôn
                                        */
                                       Late = ChamCongConstant.CheckLate(viewModel, cong, ca, miss, nv),
                                       Early = ChamCongConstant.CheckEarly(viewModel, cong, ca, miss, nv),
                                       Des = ChamCongConstant.CheckTimeDown(viewModel, miss, nv, cong.Date)
                                   };
                        if (lstFileChamCong != null)
                        {
                            lstFileChamCong = data.ToList();
                            gridView.ItemsSource = lstFileChamCong;
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void _backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Do something 
            LoadDataFromMCC();
        }
        // Completed Method 
        void _backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("Cancelled");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("Exception Thrown");
            }
            else
            {
                if (ChamCongConstant.bIsConnected)
                {
                    ChamCongConstant.axCZKEM1.Disconnect();
                    ChamCongConstant.bIsConnected = false;
                }
            }
            SetLoadingButtuon(btnLoad, false);
        }
        void SetLoadingButtuon(Button btn, bool status = true)
        {
            Application.Current.Dispatcher.BeginInvoke(
              DispatcherPriority.Background,
              new Action(() =>
              {
                  MaterialDesignThemes.Wpf.ButtonProgressAssist.SetIsIndeterminate(btn, status);
                  MaterialDesignThemes.Wpf.ButtonProgressAssist.SetIsIndicatorVisible(btn, status);
              }
            ));

        }
        #endregion

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                if (ChamCongConstant.bIsConnected)
                    ChamCongConstant.axCZKEM1.Disconnect();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
