﻿using QuanLy.ModelViews;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using QuanLy.Models;
using Syncfusion.UI.Xaml.Grid;
using System.Linq;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for PositionUC.xaml
    /// </summary>
    public partial class PositionUC : UserControl
    {
        ViewModel viewModel;
        public PositionUC()
        {
            InitializeComponent();
            this.gridView.SelectionController = new GridSelectionControllerExt(gridView);
            gridView.Height = Constant.windowHeight;
            Load();
        }
        #region GRID
        void ChangeActionOfDatagrid(bool allow = true)
        {
            gridView.AllowFiltering = allow;
            gridView.AllowSorting = allow;
        }
        #endregion
        #region EVENT
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Load();
        }

        private void Save_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Save();
        }

        private void Del_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var del = gridView.SelectedItem as Position;
                MessageBoxResult rs = MessageBox.Show("Muốn xóa dòng này", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }
                using (AppDbContext context = new AppDbContext())
                {
                    context.Positions.Remove(del);
                    context.SaveChanges();
                }
                MessageBox.Show("Xoá thành công");
                Load();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChangeActionOfDatagrid(false);
                this.gridView.View?.AddNew();
                this.gridView.View?.CommitNew();
            }
            catch { }
        }
        #endregion

        #region FUNC
        void Save()
        {
            try
            {
                var row = gridView.SelectedItem as Position;
                int Id = row?.Id ?? 0;
                if (Id == 0 && this.gridView.IsAddNewIndex(1))
                {
                    row = this.gridView.RowGenerator.Items.Find(x => x.Index == 1).RowData as Position;
                }

                if (row == null)
                    return;

                using (AppDbContext _dbContext = new AppDbContext())
                {
                    if (Id == 0)
                    {
                        row.Department = null;
                        _dbContext.Positions.Add(row);
                        _dbContext.SaveChanges();
                    }
                    else
                    {
                        //update
                        row.Department = null;
                        _dbContext.Positions.Update(row);
                        _dbContext.SaveChanges();
                    }
                }

                MessageBox.Show("Lưu thành công");
                ChangeActionOfDatagrid(true);
                Load();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        void Load()
        {
            ChangeActionOfDatagrid(true);
            try
            {
                //gridView.ClearFilters();
                viewModel = new ViewModel();
                DataContext = viewModel;
                cboDepartment.ItemsSource = viewModel.Departments;
                gridView.ItemsSource = viewModel.Positions;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        private void btnSaveAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult rs = MessageBox.Show("Muốn lưu tất cả thay đổi", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }
                using (AppDbContext _dbContext = new AppDbContext())
                {
                    var upadte = viewModel.Positions.Where(x => x.Id != 0).Select(x => { x.Department = null; return x; });
                    var insert = viewModel.Positions.Where(x => x.Id == 0).Select(x => { x.Department = null; return x; }); 
                    if (upadte != null)
                    {
                        _dbContext.Positions.UpdateRange(upadte);
                        _dbContext.SaveChanges();
                    }
                    if (insert != null)
                    {
                        //update
                        _dbContext.Positions.AddRange(insert);
                        _dbContext.SaveChanges();
                    }
                }
                MessageBox.Show("Lưu thành công");
                Load();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
    }
}
