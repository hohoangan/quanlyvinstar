﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class Department
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [JsonIgnore]
        public virtual ICollection<Position> Positions { get; set; }
        [JsonIgnore]
        public virtual ICollection<Manager> Managers { get; set; }
    }

    public class Position
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required, Id]
        public int DepartmentId { get; set; }
        [JsonIgnore]
        public Department Department { get; set; }
        [JsonIgnore]
        public virtual ICollection<StaffContract> StaffContracts { get; set; }
        [JsonIgnore]
        public virtual ICollection<QuestionForPosition> QuestionForPositions { get; set; }
    }
}
