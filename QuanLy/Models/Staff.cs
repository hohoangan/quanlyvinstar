﻿using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class Staff
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string FullName { get; set; }
        public DateTime BirthDay  { get; set; }
        public bool Sex  { get; set; }
        public string Country  { get; set; }
        public string Address  { get; set; }
        public string BHXH { get; set; }
        [Required]
        public string AddressNow  { get; set; }
        public string CMND  { get; set; }
        public string CMNDAdress  { get; set; }
        public string Phone  { get; set; }
        public string Email  { get; set; }
        public string EmailPrivate { get; set; }
        public string Contact1 { get; set; }
        public string Contact2 { get; set; }
        public DateTime CMNDCreatedDate  { get; set; }
        public string BankNo { get; set; }
        public string BankName { get; set; }
        public string TaxNo { get; set; }
        public string Status { get; set; } //1: Đang thử việc | Đã kí HĐLĐ | Đã nghỉ | Thực tập sinh | Cộng tác viên | Khác
        public string Des { get; set; } 
        public string PlateNo { get; set; } 
        public string BikeType { get; set; }
        public DateTime? ParkingFromDate { get; set; }
        public DateTime? ParkingToDate { get; set; }
        public string ParkingPlace { get; set; }
        [Required, Id]
        public int CityId { get; set; }
        [JsonIgnore]
        public City City { get; set; }
        [Required, Id]
        public int ShiftId { get; set; }
        [JsonIgnore]
        public Shift Shift { get; set; }
        [JsonIgnore]
        public virtual ICollection<StaffContract> StaffContracts { get; set; }
        [JsonIgnore]
        public virtual ICollection<Manager> Managers { get; set; }

        internal object Where()
        {
            throw new NotImplementedException();
        }
    }
}
