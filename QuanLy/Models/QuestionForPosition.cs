﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class QuestionForPosition
    {
        [Key]
        public int Id { get; set; }
        [Required, Id]
        public int PositionId { get; set; }
        [JsonIgnore]
        public virtual Position Position { get; set; }
        [Required, Id]
        public int QuestionId { get; set; }
        [JsonIgnore]
        public virtual Question Question { get; set; }
    }
}
