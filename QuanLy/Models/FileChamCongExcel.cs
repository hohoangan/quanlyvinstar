﻿using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLy.Models
{
    public class FileChamCongExcel
    {
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Date { get; set; }
        public string DayOfWeek { get; set; }
        public TimeSpan Start { get; set; }
        public TimeSpan End { get; set; }
    }
}
