﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class StaffContract
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string ContractNo { get; set; }
        public string ContractType { get; set; }
        public double BasicSalary { get; set; }
        public double Bonus { get; set; }//luong ngoai HD 
        public double AllowanceLunch { get; set; }//phụ cấp com trua
        public double AllowancePhone { get; set; }//phụ cấp dt
        public double AllowanceHouse { get; set; }//phụ cấp nha o
        public double AllowanceGas { get; set; }//phụ cấp xag
        public double AllowanceKPI { get; set; }//phụ cấp KPI
        public double AllowanceCommunicate { get; set; }//phụ cấp giao tiếp
        public double AllowanceOther { get; set; }//phụ cấp khac

        public int MonthContract { get; set; }
        [Required]
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        [Required]
        public DateTime SignatureDate { get; set; }
        public DateTime DateOfJoinBHXH { get; set; }
        [Required, Id]
        public int StaffId { get; set; }
        [JsonIgnore]
        public Staff Staff { get; set; }
        [Required, Id]
        public int PositionId { get; set; }
        [JsonIgnore]
        public Position Position { get; set; }
        [Required, Id]
        public int CompanyId { get; set; }
        [JsonIgnore]
        public Company Company { get; set; }
    }
}
