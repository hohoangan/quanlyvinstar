﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class CandidateStep//Các bước giải quyết hồ sơ 1 ứng viên
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int? ParentId { get; set; }
        [JsonIgnore]
        public virtual ICollection<CandidateStatus> CandidateStatuss { get; set; }
    }
}
