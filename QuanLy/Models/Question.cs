﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class Question
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required, Id]
        public int QuestionTypeId { get; set; }
        [JsonIgnore]
        public virtual QuestionType QuestionType { get; set; }
        [JsonIgnore]
        public virtual ICollection<QuestionForPosition> QuestionForPositions { get; set; }
    }
}
