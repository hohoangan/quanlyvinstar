﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using OfficeOpenXml;
using System.IO;
using OfficeOpenXml;
using System.Globalization;

namespace ChamCong
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<FileChamCong> lstFileChamCong;

        public MainWindow()
        {
            InitializeComponent();
        }
        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
        }

        void ReadXLS(string filePath)
        {
            lstFileChamCong = new List<FileChamCong>();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            // Mở file excel
            using (ExcelPackage excelPackage = new ExcelPackage(new FileInfo(filePath)))
            {
                // Lấy sheet đầu tiên của file excel
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets[0];
                List<string> err = new List<string>();
                // Lấy số dòng và số cột của sheet
                int rowCount = worksheet.Dimension.Rows;
                int colCount = worksheet.Dimension.Columns;

                // Duyệt qua từng dòng của sheet
                int row = 2;
                for (; row <= rowCount; row++)
                {
                    try
                    {
                        // Đọc giá trị từng ô của dòng
                        string maNhanVien = worksheet.Cells[row, 1].Value?.ToString() ?? "";
                        if (string.IsNullOrEmpty(maNhanVien))
                            break;
                        string tenNhanVien = worksheet.Cells[row, 2].Value?.ToString() ?? "";

                        string ngayThangString = worksheet.Cells[row, 3].Value?.ToString() ?? ""; // Lấy chuỗi ngày tháng từ ô của file Excel
                        DateTime ngayThang = new DateTime();
                        if (!string.IsNullOrEmpty(ngayThangString))
                        {
                            ngayThang = DateTime.ParseExact(ngayThangString, "dd/MM/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                        }
                        string thoiGianVaoString = worksheet.Cells[row, 4].Value?.ToString() ?? ""; // Lấy chuỗi thời gian vào từ ô của file Excel
                        TimeSpan thoiGianVao = new TimeSpan();
                        if (!string.IsNullOrEmpty(thoiGianVaoString))
                        {
                            DateTime dateTimeVao = DateTime.ParseExact(thoiGianVaoString, "HH:mm", CultureInfo.InvariantCulture);
                            thoiGianVao = dateTimeVao.TimeOfDay;
                        }
                        string thoiGianRaString = worksheet.Cells[row, 5].Value?.ToString() ?? ""; // Lấy chuỗi thời gian ra từ ô của file Excel
                        TimeSpan thoiGianRa = new TimeSpan();
                        if (!string.IsNullOrEmpty(thoiGianRaString))
                        {
                            DateTime dateTimeRa = DateTime.ParseExact(thoiGianRaString, "HH:mm", CultureInfo.InvariantCulture);
                            thoiGianRa = dateTimeRa.TimeOfDay;
                        }
                        var cc = new FileChamCong
                        {
                            Code = maNhanVien,
                            FullName = tenNhanVien,
                            Date = ngayThang,
                            Start = thoiGianVao,
                            End = thoiGianRa,
                            DayOfWeek = ngayThang.DayOfWeek,
                            Des = ""
                        };
                        Shift ca = new Shift
                        {
                            Start = new TimeSpan(8, 30, 00),
                            End = new TimeSpan(17, 30, 00),
                            LamThu7 = 1,
                            Sat = new TimeSpan(12, 00, 00),
                        };
                        cc.Late = CheckLate(cc, ca);
                        cc.Early = CheckEarly(cc, ca);
                        lstFileChamCong.Add(cc);
                        // Xử lý dữ liệu đã đọc được từ file excel ở đây
                    }
                    catch (Exception)
                    {
                        err.Add(row.ToString());
                    }
                }
                if (err.Count > 0)
                {
                    string er = "Những dòng excel bị lỗi đọc dữ liệu: ";
                    for (int i = 0; i < err.Count; i++)
                    {
                        er += string.Format("{0},", err[i]);
                    }
                    MessageBox.Show(er);
                }
                gridView.ItemsSource = lstFileChamCong;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel Workbook |*.xlsx|Excel 97-2003 Workbook|*.xls";
            try
            {
                if (openFileDialog.ShowDialog() == true)
                {
                    string filename = openFileDialog.FileName;
                    ReadXLS(filename);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex?.InnerException?.Message ?? ex.Message);
            }
            finally { }
        }
        #region check cong
        TimeSpan CheckLate(FileChamCong cong, Shift ca)
        {
            if (cong.DayOfWeek.Equals(DayOfWeek.Sunday) || ca.LamThu7 == 0)
                return new TimeSpan();

            //miss punch
            if (cong.Start.Equals(new TimeSpan()))
                return new TimeSpan(4, 0, 0);

            TimeSpan tresom = ca.Start < cong.Start ? cong.Start - ca.Start : new TimeSpan();
            tresom = tresom <= new TimeSpan(0, 10, 0) ? new TimeSpan() : tresom - new TimeSpan(0, 10, 0);
            TimeSpan kq = tresom;

            if (kq > new TimeSpan(4, 0, 0))
                return new TimeSpan(4, 0, 0);
            else if (kq < new TimeSpan())
                return new TimeSpan();
            return kq;
        }
        TimeSpan CheckEarly(FileChamCong cong, Shift ca)
        {
            TimeSpan End = ca.End;
            if (cong.DayOfWeek.Equals(DayOfWeek.Sunday) || ca.LamThu7 == 0)
                return new TimeSpan();

            if (cong.DayOfWeek.Equals(DayOfWeek.Saturday))
            {
                End = ca.Sat;
                if (cong.Start.Equals(new TimeSpan()))
                    return new TimeSpan();
            }

            //miss punch
            if (cong.End.Equals(new TimeSpan()))
                return new TimeSpan(4, 0, 0);

            TimeSpan tresom = End > cong.End ? End - cong.End : new TimeSpan();
            TimeSpan kq = tresom;

            if (kq > new TimeSpan(4, 0, 0))
                return new TimeSpan(4, 0, 0);
            else if (kq < new TimeSpan())
                return new TimeSpan();
            return kq;
        }
        #endregion
    }
}
