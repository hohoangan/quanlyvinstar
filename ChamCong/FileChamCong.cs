﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ChamCong
{
    public class FileChamCong
    {
        public string Code { get; set; }
        public string FullName { get; set; }
        public DateTime Date { get; set; }
        public DayOfWeek DayOfWeek { get; set; }
        public TimeSpan Start { get; set; }
        public TimeSpan End { get; set; }
        public TimeSpan Early { get; set; }
        public TimeSpan Late { get; set; }
        public TimeSpan Total { get; set; }
        public string Des { get; set; }
    }
}
