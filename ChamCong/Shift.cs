﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace ChamCong
{
    public class Shift
    {
        public TimeSpan Start { get; set; }
        public TimeSpan End { get; set; }
        public TimeSpan Sat { get; set; }
        public int LamThu7 { get; set; } //0: ko, 1:lam, 2: lam 1/2
    }
}
